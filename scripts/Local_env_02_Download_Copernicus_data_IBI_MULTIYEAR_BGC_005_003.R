# ---------------------------------------------------------------------------- #
#           SUIVI NOUR    Mise a jour de la distribution des nourricerie                 
# ---------------------------------------------------------------------------- #
# 02. Load large scale Copernicus marine data.
# ---------------------------------------------------------------------------- #


# Load : https://data.marine.copernicus.eu/product/GLOBAL_MULTIYEAR_BGC_001_029/download?dataset=cmems_mod_glo_bgc_my_0.25_P1M-m
#-Mass concentration of chlorophyll a in sea water 
#-Net primary production of biomass expressed as carbon per unit volume in sea water
#-Mole concentration of dissolved molecular oxygen in sea water
#- Mole concentration of phytoplankton expressed as carbon in sea water

source(paste0("./scripts/boot.R"))

name.var <- c( "nppv")# "chl",


date_list <- format(seq(from = as.Date("1993-01-02"),
                  to = as.Date("2021-12-28"),
                  by = 'day'))

# date_end <- format(seq(from = as.Date("1993-01-02"),
#                         to = as.Date("2021-12-28"),
#                         by = 'day'))
# 


day_month_year_list <- format(as.Date(date_list), "%Y_%m_%d")



for(var_i in name.var){
  print(paste0(var_i, " --- initiated"))
  
  n.cores <- detectCores()
  clust <- makeCluster(n.cores - 2)
  registerDoParallel(clust)
  foreach::getDoParWorkers()

  
  clusterEvalQ(clust, {
    library("raster")
    library("akima")
    library("ggplot2")
    library("terra")
    library("stringr")
    library("RColorBrewer")
    library("tidyverse")
    library('RCMEMS')
  })
  
  folder_out<- paste0('C:/Users/sfabriru/Documents/Research/SuiviNour/R/SuiviNour/SDM/data/raw/',
                      "Env_data/Copernicus/",
         "Copernicus_",
         "IBI_MULTIYEAR_BGC_005_003-TDS",
         "_",
         var_i)
  
  files_downloaded  <-list.files(paste0('C:/Users/sfabriru/Documents/Research/SuiviNour/R/SuiviNour/SDM/data/raw/',
                                        "Env_data/Copernicus/",
                                        "Copernicus_",
                                        "IBI_MULTIYEAR_BGC_005_003-TDS",
                                        "_",
                                        var_i))
  
  if(length(files_downloaded)>0){
    
    dates_downloaded <- str_sub(files_downloaded, start= -13)
    dates_downloaded <-  str_remove_all(dates_downloaded,  ".nc")
    dates_downloaded <-  str_replace_all(dates_downloaded,  "_", "-")
    date_start<-date_list[which(!(date_list %in% dates_downloaded))]
    day_month_year<-day_month_year_list[which(!(date_list %in% dates_downloaded))]
    
    
  }else{
    
    date_start<-date_list
    day_month_year<-day_month_year_list
  }
  ############## parallelize download ##############
  
  foreach(day_i = 1:(length(date_start)-1),
          .verbose = TRUE) %dopar% {
            print(paste0(date_start[day_i], "-------- initiated"))
            
  chemin_motuclient <-
    "C:/Users/sfabriru/Anaconda3/Lib/site-packages/motuclient.py"
  motu_site <-"https://my.cmems-du.eu/motu-web/Motu"# depend on the product, it can be : https://my.cmems-du.eu/motu-web/Motu
  user.id  <- "xxx" # identifiant  sur Copernicus
  password.id <- "xxx" # mot de passe sur Copernicus
  service <- "IBI_MULTIYEAR_BGC_005_003-TDS"
  product <- "cmems_mod_ibi_bgc_my_0.083deg-3D_P1D-m"
  variable <- var_i
  date_min <- date_start[day_i]# script do
  date_max <- date_start[day_i]
  auth_mode = "cas"
  zone.mtq <- c(
    ymin = min_y_loc,
    ymax = max_y_loc,
    xmin = min_x_loc,
    xmax = max_x_loc
  )  %>% as.character()
  profs <- c(0.5057600140571594,1.5558552742004395) %>% as.character()
  
  
  
  folder_out <- paste0('C:/Users/sfabriru/Documents/Research/SuiviNour/R/SuiviNour/SDM/data/raw/',
                       "Env_data/Copernicus/",
                       "Copernicus_",
                       service,
                       "_",
                       variable)
  
  
  
  dir.create(folder_out ,
             recursive = T,
             showWarnings = F)
  out_name <-
    paste0("Copernicus_",
           service,
           "_",
           variable,
           "_",
           day_month_year[day_i],
           
           ".nc")
  
    configuration <-
      CMEMS.config(
        python = 'C:/Users/sfabriru/Anaconda3/python.exe',
        script = chemin_motuclient,
        user  = user.id,
        pwd = password.id,
        motu = motu_site,
        service.id = service,
        product.id = product,
        variable = variable,
        date.min = date_min ,
        date.max = date_max ,
        latitude.min = zone.mtq[1],
        latitude.max = zone.mtq[2],
        longitude.min = zone.mtq[3],
        longitude.max = zone.mtq[4],
        depth.min = profs[1],
        depth.max = profs[2],
        out.dir = folder_out,
        out.name = out_name
      )
    
    CMEMS.download(configuration,   
                   quiet=TRUE)
    
    print(paste0(var_i, " date : ", as.character(date_min), " --- complete"))
          }
  stopCluster(clust)
  
  print(paste0(var_i, " --- completed"))
}


#################### Download oxygen 


source(paste0("./scripts/boot.R"))

name.var <- c(  "o2")


date_list <- format(seq(from = as.Date("1993-01-02"),
                        to = as.Date("2021-12-28"),
                        by = 'day'))

# date_end <- format(seq(from = as.Date("1993-01-02"),
#                         to = as.Date("2021-12-28"),
#                         by = 'day'))
# 


day_month_year_list <- format(as.Date(date_list), "%Y_%m_%d")



for(var_i in name.var){
  print(paste0(var_i, " --- initiated"))
  
  n.cores <- detectCores()
  clust <- makeCluster(n.cores - 2)
  registerDoParallel(clust)
  foreach::getDoParWorkers()
  
  
  clusterEvalQ(clust, {
    library("raster")
    library("akima")
    library("ggplot2")
    library("terra")
    library("stringr")
    library("RColorBrewer")
    library("tidyverse")
    library('RCMEMS')
  })
  
  folder_out<- paste0('C:/Users/sfabriru/Documents/Research/SuiviNour/R/SuiviNour/SDM/data/raw/',
                      "Env_data/",
                      "Copernicus_",
                      "IBI_MULTIYEAR_BGC_005_003-TDS",
                      "_",
                      var_i)
  
  files_downloaded  <-list.files(paste0('C:/Users/sfabriru/Documents/Research/SuiviNour/R/SuiviNour/SDM/data/raw/',
                                        "Env_data/Copernicus/",
                                        "Copernicus_",
                                        "IBI_MULTIYEAR_PHY_005_002-TDS",
                                        "_",
                                        var_i))
  
  if(length(files_downloaded)>0){
    
    dates_downloaded <- str_sub(files_downloaded, start= -13)
    dates_downloaded <-  str_remove_all(dates_downloaded,  ".nc")
    dates_downloaded <-  str_replace_all(dates_downloaded,  "_", "-")
    date_start<-date_list[which(!(date_list %in% dates_downloaded))]
    day_after<-as.character(as.Date(date_start)+1)
    day_month_year<-day_month_year_list[which(!(date_list %in% dates_downloaded))]
    
    
  }else{
    
    date_start<-date_list
    day_month_year<-day_month_year_list
    day_after<-as.character(as.Date(date_start)+1)
  }
  ############## parallelize download ##############
  
  foreach(day_i = 1:(length(date_start)-1),
          .verbose = TRUE) %dopar% {
            print(paste0(date_start[day_i], "-------- initiated"))
            
            chemin_motuclient <-
              "C:/Users/sfabriru/Anaconda3/Lib/site-packages/motuclient.py"
            motu_site <-"https://my.cmems-du.eu/motu-web/Motu"# depend on the product, it can be : https://my.cmems-du.eu/motu-web/Motu
            user.id  <- "xxx" # identifiant  sur Copernicus
            password.id <- "xxx" # mot de passe sur Copernicus
            service <- "IBI_MULTIYEAR_BGC_005_003-TDS"
            product <- "cmems_mod_ibi_bgc_my_0.083deg-3D_P1D-m"
            variable <- var_i
            date_min <- date_start[day_i]# script do
            date_max <- date_start[day_i]
            auth_mode = "cas"
            zone.mtq <- c(
              ymin = min_y_loc,
              ymax = max_y_loc,
              xmin = min_x_loc,
              xmax = max_x_loc
            )  %>% as.character()
            profs <- c(0.5057600140571594,565.2922973632812 ) %>% as.character()
            
            
            
            folder_out <- paste0('C:/Users/sfabriru/Documents/Research/SuiviNour/R/SuiviNour/SDM/data/raw/',
                                 "Env_data/",
                                 "Copernicus_",
                                 service,
                                 "_",
                                 variable)
            
            
            
            dir.create(folder_out ,
                       recursive = T,
                       showWarnings = F)
            out_name <-
              paste0("Copernicus_",
                     service,
                     "_",
                     variable,
                     "_",
                     day_month_year[day_i],
                     
                     ".nc")
            
            configuration <-
              CMEMS.config(
                python = 'C:/Users/sfabriru/Anaconda3/python.exe',
                script = chemin_motuclient,
                user  = user.id,
                pwd = password.id,
                motu = motu_site,
                service.id = service,
                product.id = product,
                variable = variable,
                date.min = date_min ,
                date.max = date_max ,
                latitude.min = zone.mtq[1],
                latitude.max = zone.mtq[2],
                longitude.min = zone.mtq[3],
                longitude.max = zone.mtq[4],
                depth.min = profs[1],
                depth.max = profs[2],
                out.dir = folder_out,
                out.name = out_name
              )
            
            CMEMS.download(configuration,   
                           quiet=FALSE)
            
            print(paste0(var_i, " date : ", as.character(date_min), " --- complete"))
          }
  stopCluster(clust)
  
  print(paste0(var_i, " --- completed"))
}


