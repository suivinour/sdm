### This script is here to convert the occurences data frame into a raster grid 
##
### A plot off presence and absence distribution is performed to check the potential biais

### Calling all of the common object to my analyses
source("./scripts/boot.R")

##### Load layer with the target resolution ############################################# 
# from https://data.marine.copernicus.eu/product/IBI_ANALYSISFORECAST_PHY_005_001/description

new_grid<-raster(paste0(path_to_data_raw,
                        "/Env_data/cmems_mod_ibi_phy_anfc_0.027deg-2D_PT15M-i_1674546423491.nc"))

new_grid <-extend(x=new_grid,
                  y=c(min_x_reg, 
                      max_x_reg, 
                      min_y_reg,
                      max_y_reg))# expend the grid to the target domain

new_grid<-crop(x=new_grid, 
               y=c(min_x_reg, 
                   max_x_reg, 
                   min_y_reg,
                   max_y_reg))

names(new_grid)<-'values'
values(new_grid)<-1

new_grid.df<-as_tibble(rasterToPoints(new_grid))#transform in df

################ Rasterize coastline to generate a mask on the target grid 

coast_rast <- fasterize(coastline_reg,
                        raster=new_grid)

coast_rast[is.na(coast_rast$layer)]<-999
coast_rast[coast_rast$layer==1]<-NA
#plot(coast_rast)
names(coast_rast)<-'values'

################# Apply the mask to new grid 

new_grid<-coast_rast
names(new_grid)<-'values'
new_grid<-crop(new_grid, extent(min_x_reg,
                                max_x_reg,
                                min_y_reg,
                                max_y_reg))#crop to the regional scale domain
################# Load WKT files which is a file to remove Med Sea 

WKT <- st_read(str_c(paste0(path_to_data_raw,'coastline_outline/WKT-point.shp')), quiet = TRUE)|>
  summarise(geometry = st_combine(geometry)) %>%
  st_cast("POLYGON")

new_grid_no_Med<-fasterize(WKT,raster=new_grid) #rasterize with new_grid resolution
new_grid_final<-mask(new_grid_no_Med, 
                     new_grid)# apply the mask to remove continent and get the final new_grid
names(new_grid_final)<-'values'


#########################################################################
############################## Download occurences ######################
#########################################################################



##choice of species
sp=1
### Import data

Regional_scale_occurences <- read_delim(paste0(path_to_data_tidy,'Occ/Regional/',species[sp],"_Regional_scale_occurences.csv"),delim = ",", col_select = c(2:9))


##################### Repartition presence #############################################

Presence <- Regional_scale_occurences %>% filter(Occurence == 1)
#### Creation of mean by Year ###################################

Presence <- as.data.frame(Presence)

Year <- unique(Presence$Year)
result <- list()

## Each year is separated into a specific raster layer
for (i in 1:length(Year)) {
  # get the coordinates for species i
  Presence_year <- Presence[Presence$Year == Year[i], c('Long','Lat')]    
  
  # For each cell, the value is 1 if there is at least one occurence
  result[[i]] <- rasterize(Presence_year, new_grid_final, fun = function(x, ...)1)
  
}  

# Create a raster stack
Stack_presence_year <- stack(result)

# Create one layer that compute the mean of all stack
# The use of fun = mean did not work in my case so i prefer do the sum and then 
# divide by 29, the number of stack
Mean_presence <- calc(Stack_presence_year, fun = sum, na.rm = T)
Mean_presence <- Mean_presence/29

# Create a data frame for plotting
Mean_presence_df <- as.data.frame(Mean_presence, xy = TRUE)

Mean_presence_df$layer[Mean_presence_df$layer == 0]<- NA


# ggplot()+
#   geom_raster(data= Mean_presence_df,
#               mapping = aes(x = x, y = y, fill=layer))+
#   scale_fill_viridis_c()+
#   geom_sf(data = coastline_reg, 
#           size = 3, color = "black", fill = "grey")+
#   xlim(c(-15,15))+
#   ylim(c(43, 65))+
#   ggtitle(paste0("Presence recurrence of ",species[sp])) +
#   labs(x = "Longitude",
#        y = "Latitude",
#        fill = "Recurrence")+
#   theme_bw()+
#   theme(plot.title = element_text(hjust = 0.5))
# ggsave(paste0("Presence recurrence of ",species[sp],".pdf"),width = 7, height = 5, units = "in",path = path_to_figures)

##################### Repartition absence #############################################

Absence <- Regional_scale_occurences %>% filter(Occurence == 0)

################### Spatial distribution of absence ###################################
## Je veux representer la recurrence d'observation d'un point dans le temps. Pour chaque
## pixel d'un raster, il y aura le rapport entre (nombre de presence/nombre d'annee)

Absence <- as.data.frame(Absence)

Year <- unique(Absence$Year)
result <- list()

## Each year is separated into a specific raster layer
for (i in 1:length(Year)) {
  # get the coordinates for species i
  Absence_year <- Absence[Absence$Year == Year[i], c('Long','Lat')]    
  
  # For each cell, the value is 1 if there is at least one occurence
  result[[i]] <- rasterize(Absence_year, new_grid_final, fun = function(x, ...)1)
  
}  

# Create a raster stack
Stack_Absence_year <- stack(result)

# Create one layer that compute the mean of all stack
# The use of fun = mean did not work in my case so i prefer do the sum and then 
# divide by 29, the number of stack
Mean_Absence <- calc(Stack_Absence_year, fun = sum, na.rm = T)
Mean_Absence <- Mean_Absence/29

# Create a data frame for plotting
Mean_Absence_df <- as.data.frame(Mean_Absence, xy = TRUE)

Mean_Absence_df$layer[Mean_Absence_df$layer == 0]<- NA


ggplot()+
  geom_raster(data= Mean_Absence_df,
              mapping = aes(x = x, y = y, fill=layer), )+
  scale_fill_viridis_c()+
  geom_sf(data = coastline_reg, 
          size = 3, color = "black", fill = "grey")+
  xlim(c(-15,15))+
  ylim(c(43, 65))+
  ggtitle(paste0("Absence recurrence of ",species[sp])) +
  labs(x = "Longitude",
       y = "Latitude",
       fill = "Recurrence")+
  theme_bw()+
  theme(plot.title = element_text(hjust = 0.5))
ggsave(paste0("Absence recurrence of ",species[sp],".pdf"),width = 7, height = 5, units = "in",path = path_to_figures)


