######################################################################################
### ABA / EMH Avril 2014 ###   R 3.0.2 x64
# Script pour r�cup�rer les donn�es issues d'un fichier Netcdf et transformer sous forme d'une table (LONG, LAT, valeur) - AUTOMATISATION extraction fichiers annuels
# Donn�es pour Perseus r�cup�r�es :   http://www.myocean.eu/web/24-catalogue.php
# Username: abrindamour
# Password: Td8WZsyg
# MAJ Juin2014

#mole_concentration_of_nitrate_in_sea_water
#mass_concentration_of_chlorophyll_in_sea_water
#mole_concentration_of_phosphate_in_sea_water
#tendency_of_mole_concentration_of_particulate_organic_matter_expressed_as_carbon_in_sea_water_due_to_net_primary_production
#mole_concentration_of_phytoplankton_expressed_as_carbon_in_sea_water
#mole_concentration_of_dissolved_molecular_oxygen_in_sea_water 
######################################################################################
# Chargement des Packages
library(ncdf)

##### lecture des fichiers

################################
################################
## Temperature and Salinity
################################
################################
rm(list = ls(all=TRUE))


# S�lection du r�pertoire de travail
path <- "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All"
#path <- "E:/ABA_Nantes/BDD/Mediterranean_Models/Med_Other"
path2 <- "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/"
#path2 <- "E:/ABA_Nantes/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/"
path3 <- "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/"
#path3 <- "E:/ABA_Nantes/BDD/Mediterranean_Models/Data_All_Juin/"

setwd(path) # choix du r�pertoire de travail

##### lecture des fichiers
listFiles <- list.files(path = path3) 

# Efface tous les objets pr�c�dents
options(graphics.record=TRUE) # permet de conserver les graphiques (utiliser la commande historique)

######
### SELECTION DE LA VARIABLE
######
# "temp_fond", "PPI", "sal_surf", "h_eaudouce", "chlorophylle", "friction_fond" 
# VAR <- "PPI" ## s�lection variable pour l'identifiant utiliser la fonction 'print.ncdf(nc)'
LISTVAR <- c("TEMP","PSAL")

# value.null1 <- 0 ## selection de la valeur nulle !!! A VERIFIER SUR NOUVEAUX FICHIERS 2009 ????
value.null2 <- -999 ## selection de la valeur manquante
value.null3 <- 1.00000002004088e+20 #1e+30 ## selection de la valeur manquante  !!! A VERIFIER SUR NOUVEAUX FICHIERS 2009 ????

######
## SELECTION PLAGE ANNUELLE  
######
LISTYEAR <-  c(1990:2012)

######
## SELECTION PLAGE MENSUELLE
######
LISTMONTH <-  c("01","02","03","04","05","06","07","08","09","10","11","12")

######                                         19990101_mm-INGV--TEMP-MFSs4a4-MED-b20111221_re-fv02.00.nc
## SELECTION PLAGE ANNUELLE-MENSUELLE          19990101_mm-INGV--TEMP-MFSs4a4-MED-b20120213_re-fv02.00.nc
######
LIST_YEARMONTH <- c(paste(rep(LISTYEAR,each=12),LISTMONTH,"01",sep=""))     # 19900101


##SELECTION DES SECTEURS
sect <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_juin/R-ExtractDATA/secteurMed.csv", dec='.',sep=";", row.names=1)
LISTSECTOR<- sect$Code_Secteur


######
### OUVERTURE/TRAITEMENT DES DONNEES ncdf : boucle sur l'ensemble des donnees (TEMP et SAL)
######
for (s in LISTSECTOR[1]){
for (a in LIST_YEARMONTH) {
for (j in LISTVAR){

# SELECTION / Ouverture du fichier ncdf
setwd(path3)
nc <- open.ncdf(paste(a,"_mm-INGV--",j,"-MFSs4b3-MED-b20130712_re-fv04.00.nc",sep=""), readunlim=TRUE)

print(nc) 
 
# Nombre de variables dans le fichier ncdf
print(paste("The file has", nc$nvars,"variables"))
k<-nc$nvars

# Affichage Structure du fichier ncdf
print.ncdf(nc)

# SELECTION DE LA VARIABLE D'INTERET
#for (VAR in LISTVAR[1]) {
vardata <- get.var.ncdf(nc,start=c(1,1,1,1), count=c(-1,-1,-1,-1))#get.var.ncdf(nc, VAR, start=c(1,1,1), count=c(-1,-1,-1)) # by default, reads ALL the data
dim(vardata)

## dimensions
# Spatiale
lat <-get.var.ncdf(nc, varid="lat", start=1, count=-1)
dim(lat)
lon <-get.var.ncdf(nc, varid="lon", start=1, count=-1)
dim(lon)
# Depth
depth <- get.var.ncdf(nc, varid="depth", start=1, count=-1)
dim(depth)

# creation de la matrice 3 dimensions vardata(lat, lon, time)
dimnames(vardata) <- list(lon, lat, depth)
vardata <- vardata[ , order(as.numeric(colnames(vardata))), ]
dimnames(vardata)

dim(vardata)
nrow(vardata)
ncol(vardata)
list(depth)

colnames(vardata)
close.ncdf(nc) # fermeture du fichier ncdf source apr�s lecture des donn�es (lib�re espace m�moire !!!)

#########
## SELECTION DES SECTEURS
#########
ImportFileSector <- function(fileData, sector) { # sector has to be in "" : "GSA7"  "GSA8"  "GSA9"  "GSA10" "GSA11" "GSA16" "GSA17" "GSA18" "GSA19" "GSA20" "GSA22"
   sect <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_juin/R-ExtractDATA/secteurMed.csv", dec='.',sep=";", row.names=1)
   sect <- data.frame(sect)
   sect$Code_Secteur <- rownames(sect)
   xvil <- as.numeric(sect[sect$Code_Secteur==sector,1:2]) # c(minLONG,maxLONG) cadre geographique pour la Vilaine
   yvil <- as.numeric(sect[sect$Code_Secteur==sector,3:4])
   fVil <- fileData # read.table(paste(fileData,listName,sep="/") , sep = ";", header = T, dec = ",") 
   fileVil <- fVil[lon >= xvil[1] & lon <= xvil[2] , lat <= yvil[2] & lat >= yvil[1],]       
   return(fileVil)
}

vardatai <- ImportFileSector(fileData=vardata,sector=s)#vardata #

##########
## SELECTION DE LA PROFONDEUR PAR GRID CELL
##########
# SELECTION de la profondeur
x.func <- function(x)  {
  y <- which(!is.na(x))
  length(y)
}
d.select <- apply(vardatai, c(1,2), x.func) ; d.select[d.select == 0] <- NA

val <- c()#vector(mode="list")
for (i in 1:length(d.select)){  #171281
  val[i] <- sapply(d.select[i], function(x) vardatai[,,x])[i] 
}

vardata.j <- array(val,dim=dim(d.select),dimnames=list(rownames(d.select),colnames(d.select)))

######################################
#comparer le temps de calcul pour optimiser code
#ptm <- proc.time()
#val <- c()#vector(mode="list")
#for (i in 1:length(d.select)){  #171281
#  val[i] <- sapply(d.select[i], function(x) vardatai[,,x])[i] 
#}
#val
#proc.time() - ptm

#ptm2 <- proc.time()
#tmp.vec = matrix(as.vector(vardatai),nrow=length(vardatai[,,1]),ncol=dim(vardatai)[3]) ; colnames(tmp.vec) <- c(paste("depth",1:dim(vardatai)[3],sep=""))
#mat = data.frame(cbind(tmp.vec,lon,lat))
#val <- head(diag(as.matrix(mat[,c(d.select)])))
#val
#proc.time() - ptm2

######################### not run
#ptm <- proc.time()
#YY=matrix(c(runif(10),runif(10)),nrow=4, ncol=5)
#YY= array(YY,dim=c(4,5,3))
#rt= c(1,1,2,1,2,3,3,3,2,1,1,1,2,1,2,3,1,3,2,1)
#for (i in 1:length(rt)){  #171281
#  toto[i] <- sapply(rt[i], function(x) YY[,,x])[i] 
#}
#toto
#proc.time() - ptm


#ptm2 <- proc.time()
#YY=matrix(c(runif(10),runif(10)),nrow=4, ncol=5)
#YY= array(YY,dim=c(4,5,3))
#rt= c(1,1,2,1,2,3,3,3,2,1,1,1,2,1,2,3,1,3,2,1)
#tmp.vec = matrix(as.vector(YY),nrow=length(YY[,,1]),ncol=dim(YY)[3]) ; colnames(tmp.vec) <- c(paste("depth",1:dim(YY)[3],sep=""))
#mat = data.frame(tmp.vec) #data.frame(cbind(tmp.vec,lon,lat))  
#val <- diag(as.matrix(mat[,rt]))
#val
#proc.time() - ptm2
#########################################


#sapply(c(d.select)[98935:98939], function(x) vardatai[352:353,117:118,x], simplify="array") 

#####
### CREATION DE LA TABLE EXPORT ET ENREGISTREMENT EN FICHIER .txt
#####
long<-rownames(vardata.j) 
lati<-colnames(vardata.j)
value<-as.data.frame(matrix(vardata.j))
coord<-merge(long,lati)
sector<-rep(paste(s,sep=""),nrow(coord)) 

value.export<-cbind(coord,value) #cbind(coord,value,sector)
# value.export<-subset(value.export, value.export$V1!=value.null1)
value.export<-subset(value.export, value.export$V1!=value.null2)
value.export<-subset(value.export, value.export$V1!=value.null3)
colnames(value.export)<- list("LONG","LAT",paste(j,sep=""))#list("LONG","LAT",paste(j,sep=""),"GSA")
value.export$Year <- rep(substr(a, 1, 4),each=nrow(value.export))
value.export$Month <- rep(substr(a, 5, 6),each=nrow(value.export))
value.export
# create an text output file
fname <- paste(s,"_",j,a,".txt", sep="")   #paste(j,a,".txt", sep="")   #paste(s,"_",j,a,".txt", sep="")      

# write the content of the variable 'stations' to the external output file
setwd(paste(path2,sep="")) # choix du r�pertoire de travail
  write.table(value.export, fname, append = FALSE, quote = FALSE, sep = ";",
                   eol = "\n", na = "NA", dec = ".", row.names = FALSE,
                   col.names = TRUE)
}
}
}   #sector loop

################################
################################
## c("nit","chl","npp","dox")
################################
################################

rm(list = ls(all=TRUE))

library(ncdf)


# S�lection du r�pertoire de travail
path <- "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All"
path2 <- "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/"
path3 <- "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/"

setwd(path3) # choix du r�pertoire de travail

# Efface tous les objets pr�c�dents
options(graphics.record=TRUE) # permet de conserver les graphiques (utiliser la commande historique)


######
### SELECTION DE LA VARIABLE
######
# 2001-2005 / 2006-2010
LISTVAR <- c("nit","chl","npp","dox")
# value.null1 <- 0 ## selection de la valeur nulle !!! A VERIFIER SUR NOUVEAUX FICHIERS 2009 ????
value.null2 <- -999 ## selection de la valeur manquante
value.null3 <- 1.00000002004088e+20 #1e+30 ## selection de la valeur manquante  !!! A VERIFIER SUR NOUVEAUX FICHIERS 2009 ????

######
## SELECTION PLAGE ANNUELLE MAJ des donn�es sur Data_All_Juin  
######
LISTYEAR <-  c(1999:2010)

######
## SELECTION PLAGE MENSUELLE
######
LISTMONTH <- c("01","02","03","04","05","06","07","08","09","10","11","12")

######                                         19990101_mm-INGV--TEMP-MFSs4a4-MED-b20111221_re-fv02.00.nc
## SELECTION PLAGE ANNUELLE-MENSUELLE          19990101_mm-INGV--TEMP-MFSs4a4-MED-b20120213_re-fv02.00.nc
######

LIST_YEARMONTH <- c(paste(rep(LISTYEAR,each=12),LISTMONTH,"01",sep=""))     # 19900101


##SELECTION DES SECTEURS
sect <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_juin/R-ExtractDATA/secteurMed.csv", dec='.',sep=";", row.names=1)
LISTSECTOR<- sect$Code_Secteur


######
### OUVERTURE/TRAITEMENT DES DONNEES ncdf
######
for (s in LISTSECTOR){
for (a in LIST_YEARMONTH) {
setwd(path3) 
# SELECTION / Ouverture du fichier ncdf

nc <- open.ncdf(paste(a,"_mm-OGS---opatm_bfm3-MED-b20131201_re-fv04.00.nc",sep=""), readunlim=TRUE)
                       
#nc <- open.ncdf(paste(a,"_mm-INGV--PSAL-MFSs4a4-MED-b20111221_re-fv02.00.nc",sep=""), readunlim=TRUE)
print(nc) 
 
# Nombre de variables dans le fichier ncdf
print(paste("The file has", nc$nvars,"variables"))
k<-nc$nvars

# Affichage Structure du fichier ncdf
print.ncdf(nc)

# SELECTION DE LA VARIABLE D'INTERET
for (j in LISTVAR) {
vardata <- get.var.ncdf(nc,j,start=c(1,1,1,1), count=c(-1,-1,-1,-1))#get.var.ncdf(nc, VAR, start=c(1,1,1), count=c(-1,-1,-1)) # by default, reads ALL the data
dim(vardata)

## dimensions
# Spatiale
lat <-get.var.ncdf(nc, varid="latitude", start=1, count=-1)
dim(lat)
lon <-get.var.ncdf(nc, varid="longitude", start=1, count=-1)
dim(lon)
# Depth
depth <- get.var.ncdf(nc, varid="depth", start=1, count=-1)
dim(depth)
# Time
times <- get.var.ncdf(nc, varid="time", start=1, count=-1)
dim(times)                  

############### ancien
# creation de la matrice 3 dimensions vardata(lat, lon, time)
dimnames(vardata) <- list(lon, lat, depth)
vardata <- vardata[ , order(as.numeric(colnames(vardata))),]
dimnames(vardata)

dim(vardata)
nrow(vardata)
ncol(vardata)
list(depth)

colnames(vardata)

#########
## SELECTION DES SECTEURS
#########
ImportFileSector <- function(fileData, sector) { # sector has to be in "" : "GSA7"  "GSA8"  "GSA9"  "GSA10" "GSA11" "GSA16" "GSA17" "GSA18" "GSA19" "GSA20" "GSA22"
   sect <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_juin/R-ExtractDATA/secteurMed.csv", dec='.',sep=";", row.names=1)
   sect <- data.frame(sect)
   sect$Code_Secteur <- rownames(sect)
   xvil <- as.numeric(sect[sect$Code_Secteur==sector,1:2]) # c(minLONG,maxLONG) cadre geographique pour la Vilaine
   yvil <- as.numeric(sect[sect$Code_Secteur==sector,3:4])
   fVil <- fileData # read.table(paste(fileData,listName,sep="/") , sep = ";", header = T, dec = ",") 
   fileVil <- fVil[lon >= xvil[1] & lon <= xvil[2] , lat <= yvil[2] & lat >= yvil[1],]       
   return(fileVil)
}

vardatai <- ImportFileSector(fileData=vardata,sector=s)

##########
## SELECTION DE LA PROFONDEUR PAR GRID CELL
##########
# SELECTION de la profondeur
#  x.func <- function(x)  {
#    y <- which(!is.na(x))
#    length(y)
#  }
#  d.select <- apply(vardatai, c(1,2), x.func) ; d.select[d.select == 0] <- NA
#  
#  val <- c()#vector(mode="list")
#  for (i in 1:length(d.select)){  #171281
#    val[i] <- sapply(d.select[i], function(x) vardatai[,,x])[i] 
#  }
  
#  vardata.j <- array(val,dim=dim(d.select),dimnames=list(rownames(d.select),colnames(d.select)))

vardata.j <- vardatai[,,1]

#####
### CREATION DE LA TABLE EXPORT ET ENREGISTREMENT EN FICHIER .txt
#####
#value.export2 = NULL
#for (m in 1:dim(times)){
long<-rownames(vardata.j) 
lati<-colnames(vardata.j)
value<-as.data.frame(matrix(vardata.j))
coord<-merge(long,lati)
sector<-rep(paste(s,sep=""),nrow(coord)) 
value.export = NULL
value.export<-cbind(coord,value,sector)

# value.export<-subset(value.export, value.export$V1!=value.null1)
value.export<-subset(value.export, value.export$V1!=value.null2)
value.export<-subset(value.export, value.export$V1!=value.null3)
colnames(value.export)<-list("LONG","LAT",paste(j,sep=""),"GSA")
value.export$Year <- rep(substr(a, 1, 4),each=nrow(value.export))
value.export$Month <- rep(substr(a, 5, 6),each=nrow(value.export))
value.export
# create an text output file
fname <- paste(s,j,a,".txt", sep="")      
# fname <- "essai.txt"

# write the content of the variable 'stations' to the external output file
setwd(paste(path2,sep="")) # choix du r�pertoire de travail
  write.table(value.export, fname, append = FALSE, quote = FALSE, sep = ";",
                   eol = "\n", na = "NA", dec = ".", row.names = FALSE,
                   col.names = TRUE)
}
} 
}

######################################################################################
######################################################################################
## PART 2: R�cup�rer les tableaux de donn�es pour analyses (s�lection de zone g�ographique)
## Nitrate and Chla : 2001-2011     Temperature and Salinity : 1999-2011
######################################################################################
######################################################################################
library(tcltk); library(vegan); library(lattice); library(sp); library(geoR); library(fields)   #; library(spatstat)
library(spdep); library(tripack); library(vegan); library(gmp) 
library(akima); library(maps); library(gplots); library(gstat); library(PBSmapping)

print(date())

  #tclvalue(tkchooseDirectory(title="S�lectionne le r�pertoire de travail ou se trouvent les donn�es de modele", initialdir=getwd())) 
  repertoire_travail <- "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/"
  #repertoire_travail <- "E:/ABA_Nantes/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/"  #maison
  setwd(repertoire_travail)
  sect <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_juin/R-ExtractDATA/secteurMed.csv", dec='.',sep=";", row.names=1)
  

  #sect <- read.csv("E:/ABA_Nantes/BDD/Mediterranean_Models/Data_All/R-ExtractDATA/secteurMed.csv", dec='.',sep=";", row.names=1)
  LISTSECTOR<- sect$Code_Secteur

  #ListVarAll <- list.files(repertoire_travail)   #to check if a file was forgotten or not
                 #list.files("H:/ABA_Nantes/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA")

  LISTYEAR <- 1990:2012
  LISTMONTH <- c("01","02","03","04","05","06","07","08","09","10","11","12")
  LIST_YEARMONTH <- c(paste(rep(LISTYEAR,each=12),LISTMONTH,"01",sep=""))
  LISTVAR <- rep(c("TEMP", "PSAL"),each=length(LIST_YEARMONTH))
  listVar1T <- paste(rep(LISTSECTOR[1], each=552),"_",LISTVAR[1],LIST_YEARMONTH,sep="")
  listVar1S <- paste(rep(LISTSECTOR[1], each=552),"_","PSAL",LIST_YEARMONTH,sep="")
  
  LISTYEAR2 <-  c(1999:2010)
  LISTMONTH2 <- c("01","02","03","04","05","06","07","08","09","10","11","12")
  LIST_YEARMONTH2 <- c(paste(rep(LISTYEAR2,each=12),LISTMONTH2,"01",sep=""))
  LISTVAR2 <- rep(c("nit","chl","npp","dox"),each=length(LIST_YEARMONTH2))
  listVar2 <- paste(rep(LISTSECTOR, each=576),LISTVAR2,LIST_YEARMONTH2,sep="")

### Data Files
 
  fileTemp5=NULL     #listVar1: TEMP=seq(from=1, to=6624, by=276*2)  ; PSAL=seq(from=277, to=6624, by=276*2)
  for (i in listVar1T){
    tempT <- read.table(paste(repertoire_travail,i,".txt",sep="") , sep = ";", header = T, dec = ".") 
    fileTemp5 <- rbind(fileTemp5,tempT)
  }
  write.table(fileTemp5,"fileTemp5.csv",dec=".",sep=";")

save.image("E:/ABA_Nantes/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/DataNov2014.RData")
                                                                                                 
  fileSal5=NULL
  for (i in listVar1S){
    tempS <- read.table(paste(repertoire_travail,i,".txt",sep="") , sep = ";", header = T, dec = ".") 
    fileSal5 <- rbind(fileSal5,tempS)
  }   
   write.table(fileSal5,"fileSal5.csv",dec=".",sep=";")
   
save.image("F:\\NANTES_ABA\\BDD\\Mediterranean_Models\\Data_All_Juin\\R-ExtractDATA\\DataNov2014.RData")

  fileNit6=NULL     #listVar2: nit=seq(from=1, to=6912, by=144*4)  ; chl=seq(from=145, to=6912, by=144*4)   ; npp=seq(from=289, to=6912, by=144*4)  ; dox=seq(from=433, to=6912, by=144*4)  
  for (i in c(5761:5904) ){
    tempN <- read.table(paste(repertoire_travail,listVar2[i],".txt",sep="") , sep = ";", header = T, dec = ".") 
    fileNit6 <- rbind(fileNit6,tempN)
  }
  write.table(fileNit,"fileNit.csv",dec=".",sep=";")
save.image("F:/ABA_Nantes/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/DataMay2016.RData")

  
  fileNPP5=NULL
  for (i in seq(from=289, to=6912, by=144*4)){
    tempNP <- read.table(paste(repertoire_travail,listVar2[i],".txt",sep="") , sep = ";", header = T, dec = ".") 
    fileNPP5 <- rbind(fileNPP5,tempNP)
  }
  write.table(fileNPP5,"fileNPP5.csv",dec=".",sep=";")
save.image("F:/ABA_Nantes/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/DataMay2016.RData")


  fileDox5=NULL
  for (i in 433:576){
    tempD <- read.table(paste(repertoire_travail,listVar2[i],".txt",sep="") , sep = ";", header = T, dec = ".") 
    fileDox5 <- rbind(fileDox5,tempD)
  }
  write.table(fileDox5,"fileDox5.csv",dec=".",sep=";")
save.image("F:/ABA_Nantes/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/DataMay2016.RData")

  fileChl5=NULL
  for (i in seq(from=145, to=6912, by=144*4)){
    tempCH <- read.table(paste(repertoire_travail,listVar2[i],".txt",sep="") , sep = ";", header = T, dec = ".") 
    fileChl5 <- rbind(fileChl5,tempCH)
  }
  write.table(fileChl5,"fileChl5.csv",dec=".",sep=";")
save.image("F:/ABA_Nantes/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/DataMay2016.RData")


  fileChli <- rbind(fileChl, fileChl5); write.table(fileChli, "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileChl.csv", dec=".", sep=";")
  fileDoxi <- rbind(fileDox, fileDox5); write.table(fileDoxi, "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileDox.csv", dec=".", sep=";")
  fileNiti <- rbind(fileNit, fileNit5); write.table(fileNiti, "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileNit.csv", dec=".", sep=";")
  fileNPPi <- rbind(fileNPP, fileNPP5); write.table(fileNPPi, "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileNPP.csv", dec=".", sep=";")    
  fileSali <- rbind(fileSal, fileSal5i); write.table(fileSali, "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileSal.csv", dec=".", sep=";")
  fileTempi <- rbind(fileTemp, fileTemp5i); write.table(fileTempi, "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileTemp.csv",dec=".", sep=";")
  save.image("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data2015.RData")

  fileChl=read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileChl.csv", dec=".", sep=";", h=T)
  fileDox=read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileDox.csv", dec=".", sep=";", h=T)
  fileNit=read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileNit.csv", dec=".", sep=";", h=T)
  fileNPP=read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileNPP.csv", dec=".", sep=";", h=T)
  fileSal=read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileSal.csv", dec=".", sep=";", h=T)
  fileTemp=read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileTemp.csv", dec=".", sep=";", h=T)
  save.image("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data2015.RData")



# SELECTION DES DONNEES ENVIRONNEMENTALES SUR LES STATIONS D'ECHANTILLONNAGE
# Function to get the sample value within a grid // Mean
######### FUNCTIONS POUR medits

##
##############get value function
# Function to get the sample value within a grid // Mean
# Function to get the sample value within a grid // Mean
 GetValue <- function(datafile,year,month,Ref,secto) {
    library(PBSmapping)
    #Ref <- read.csv2("Reference_ST.csv", dec='.')

    # Create a grid of the bay of Vilaine using PBSmapping
    x <- datafile[datafile$Month==month & datafile$Year==year & datafile$GSA==secto,1]
    y <- datafile[datafile$Month==month & datafile$Year==year & datafile$GSA==secto,2]
    if(length(x)==0){res=rep(NA,7); names(res) <- c("EID","X.x","Y.x","Z","Year","Month","GSA")}
    if(length(x)!=0){
    xr <- abs(x[1]-x[2])#(max(x)-min(x))/20
    yr <- abs(x[1]-x[2])#(max(y)-min(y))/20   
    polys <- makeGrid(x=seq(min(x)-xr,max(x)+xr,by=xr),y=seq(min(y)-yr,max(y)+yr,by=yr),projection=1)
    plotPolys(polys, xlim=range(polys$X)+c(-0.1, 0.1), ylim=range(polys$Y)+c(-0.1, 0.1),projection=1,main=paste("Grid within the",secto,year,month))
    points(x,y,col="red")
    
    # Get the grid positions of the positions of the sampling surveys    
    xsamp <- (Ref[Ref$Year==year & Ref$Survey==secto , 'Long'])       
    ysamp <- (Ref[Ref$Year==year & Ref$Survey==secto , 'Lat'])      
    zvalue <- c(1:length(xsamp))
    eventsamp <- data.frame(EID=zvalue, X=xsamp, Y=ysamp)
    eventsamp <- as.EventData(eventsamp, projection=1)
    addPoints(eventsamp, col="black", pch=19,cex=0.6)
    text(eventsamp[,2:3],lab=as.character(eventsamp[,1]),cex=1)
    #map("france",resolution=0,add=T,interior=T, fill=T,col="grey",boundary=F )
    fcsamp <- findCells(eventsamp, polys)
    fcsamp <- data.frame(fcsamp[order(fcsamp$EID, fcsamp$PID, fcsamp$SID), ])
    fcsamp$label <- paste(fcsamp$PID, fcsamp$SID, sep="_")

    # Define the variable values for each square of the grid
    x <- x
    y <- y
    z <- datafile[datafile$Month==month & datafile$Year==year & datafile$GSA==secto,3]
    w <- 1:length(x)
    events <- data.frame(EID=w, X=x, Y=y, Z=z)
    events <- as.EventData(events, projection=1)                                     
    fc <- findCells(events, polys)
    fc <- data.frame(fc[order(fc$EID, fc$PID, fc$SID), ])
    fc$label <- paste(fc$PID, fc$SID, sep="_")
    fcc <- merge(fc,events,by.x="EID", by.y="EID")
#    fcSites <- data.frame(cbind(fcc[match(fcsamp$label, fcc$label, nomatch = 0),],fcsamp$EID))    
    # if mismatch take the value of the consecutive station
    if (length(which(match(fcsamp$label, fcc$label, nomatch = 0)==0))!=0){
      eventsampi <- eventsamp
      totoi <- eventsampi[which(match(fcsamp$label, fcc$label, nomatch = 0)==0),1] - 4     
      eventsampi[which(match(fcsamp$label, fcc$label, nomatch = 0)==0),2:3] <- eventsampi[totoi,2:3]+(xr/100)
      fcsampi <- findCells(eventsampi, polys)
      fcsampi <- data.frame(fcsampi[order(fcsampi$EID, fcsampi$PID, fcsampi$SID), ])
      fcsampi$label <- paste(fcsampi$PID, fcsampi$SID, sep="_")    
      if (length(which(match(fcsampi$label, fcc$label, nomatch = 0)==0))!=0){
        fcsampi <- fcsampi[-c(which(match(fcsampi$label, fcc$label, nomatch = 0)==0)),]
        fcSites <- data.frame(cbind(fcc[match(fcsampi$label, fcc$label, nomatch = 0),],fcsampi$EID)) #fcsampi$EID       
        fcSitesAllt <- fcSites       # generique pour associer point plus pres
        fcSitesAll <- data.frame(cbind(fcSitesAllt,EIDsamp=c(fcsampi$EID)))
      } else {
        fcSites <- data.frame(cbind(fcc[match(fcsampi$label, fcc$label, nomatch = 0),],fcsampi$EID))
        fcSitesAllt <- fcSites       # generique pour associer point plus pres
        fcSitesAll <- data.frame(cbind(fcSitesAllt,EIDsamp=c(fcsamp$EID)))
      }
    } else {
      fcSites <- data.frame(cbind(fcc[match(fcsamp$label, fcc$label, nomatch = 0),],fcsamp$EID))
      fcSitesAllt <- fcSites       # generique pour associer point plus pres
      fcSitesAll <- data.frame(cbind(fcSitesAllt,EIDsamp=c(fcsamp$EID)))
    }
    #EIDextgrid <- eventsamp[xsamp >= max(x) | ysamp >= max(y), 1]
    valueSites <- fcSitesAll[,-9]
    valSamp <- merge(eventsamp, valueSites, by.x="EID", by.y="EIDsamp")
    res=cbind(valSamp[,c(1:3,11)],Year=rep(paste(year),nrow(valSamp)),Month=rep(paste(month),nrow(valSamp)),GSA=rep(paste(secto),nrow(valSamp)))
    }
    return(res)
  }

## SELECTION PAR ZONE
source("F:\\NANTES_ABA\\BDD\\Mediterranean_Models\\FunctionsMedits.r")
#load("F:\\NANTES_ABA\\BDD\\Mediterranean_Models\\Data_All_Juin\\R-ExtractDATA\\Data2015.RData")
  repertoire_travail <- "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/"
  #repertoire_travail <- "E:/ABA_Nantes/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/"  #maison
  setwd(repertoire_travail)
  sect <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_juin/R-ExtractDATA/secteurMed2.csv", dec='.',sep=";")
  ref <- read.csv("F:/NANTES_ABA/PROJETS/Programme_IFREMER/MEDITS/Mars2013/Data_Medits/Coord_Survey.csv", h=T, dec=".", sep=";") 
  LISTSECTOR<- sect$Code_Secteur
      
ValNit6=NULL
   for (k in as.factor(LISTSECTOR)){ 
    for (j in unique(fileNit$Year)) {
     for (i in 1:12) {  
      tempNit <- GetValue_mod(datafile=fileNit,year=j,month=i,Ref=ref,secto=k)
      ValNit6 <- rbind(ValNit6,tempNit)
  }}}
write.table(ValNit6,"ValNit.csv",dec=".",sep=";",append=TRUE)   
rm(tempNit,i,j,k)  


ValNPP=NULL
   for (k in LISTSECTOR){ 
    for (j in unique(fileNPP$Year)) {
     for (i in 1:12) {  
      tempNPP <-GetValue_mod(datafile=fileNPP,year=j,month=i,Ref=ref,secto=k)
      ValNPP <- rbind(ValNPP,tempNPP)
  }}}  
write.table(ValNPP,"ValNPP.csv",dec=".",sep=";",append=TRUE) 
rm(tempNPP,i,j,k)  

                                
ValDox=NULL
   for (k in LISTSECTOR){ 
    for (j in unique(fileDox$Year)) {
     for (i in 1:12) {  
      tempDox <- GetValue_mod(datafile=fileDox,year=j,month=i,Ref=ref,secto=k)
      ValDox <- rbind(ValDox,tempDox)
  }}}  
write.table(ValDox,"ValDox.csv",dec=".",sep=";",append=TRUE) 
rm(tempDox,i,j,k)  


ValChl=NULL
   for (k in LISTSECTOR){ 
    for (j in unique(fileChl$Year)) {
     for (i in 1:12) {  
      tempChl <-   GetValue_mod(datafile=fileChl,year=j,month=i,Ref=ref,secto=k)
      ValChl <- rbind(ValChl,tempChl)
  }}}  
write.table(ValChl,"ValChl.csv",dec=".",sep=";",append=TRUE) 
rm(tempChl,i,j,k) 

                                                         ############### gsa22 et 23 until here
ValTemp=NULL
   for (k in LISTSECTOR[12:13]){   #LISTSECTOR[-c(3,11:12)]   LISTSECTOR[-c(11:12)]
    for (j in unique(fileTemp$Year)) {  #
     for (i in 1:12) {  
      tempTemp <-   GetValue_mod(datafile=fileTemp,year=j,month=i,Ref=ref,secto=k)
      ValTemp <- rbind(ValTemp,tempTemp)
  }}}  
write.table(ValTemp,"F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/ValSal.csv",dec=".",sep=";") 
rm(tempTemp,i,j,k)  

ValSal=NULL
   for (k in LISTSECTOR[12:13]){   #LISTSECTOR[11:12]
    for (j in unique(fileSal$Year)) {
     for (i in 1:12) {  
      tempSal <-   GetValue_mod(datafile=fileSal,year=j,month=i,Ref=ref,secto=k)
      ValSal <- rbind(ValSal,tempSal)
  }}}  
write.table(ValSal,"F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/ValSal.csv",dec=".",sep=";") 
save(ValTemp, file = "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/ValTemp.RData")
rm(tempSal,i,j,k)
save.image("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data2015.RData")
  

  
ValSal=ValSal[-c(which(ValSal[,4]==0)),]
fileSal=ValSal[-c(which(fileSal[,4]==0)),]
################################################################################################################################
####################################### Analyses fichiers de'environnement #####################################################
################################################################################################################################  
  ValChl22 <- ValChl
  ValDox22 <- ValDox
  ValNit22 <- ValNit
  ValNPP22 <- ValNPP
  ValSal22 <- ValSal
  ValTemp22 <- ValTemp
  
  rm(ValChl,ValDox,ValNit,ValNPP,ValSal,ValTemp)
  
  repertoire_travail <- "F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values"
  setwd(repertoire_travail)
  #datafile : "fileChl" "fileDox" "fileNit"   "fileNPP"   "fileSal"  "fileTemp"
  ValChl <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/ValChl.csv", h=T, dec=".", sep=";",row.names=NULL)  ;  ValChl <- na.omit(ValChl[,-1]) ; colnames(ValChl) <- c("EID","LONG","LAT","Chl","Year","Month","GSA")
  ValDox <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/ValDox.csv", h=T, dec=".", sep=";",row.names=NULL)  ;  ValDox <- na.omit(ValDox[,-1]) ; colnames(ValDox) <- c("EID","LONG","LAT","Dox","Year","Month","GSA")
  ValNit <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/ValNit.csv", h=T, dec=".", sep=";",row.names=NULL)  ;  ValNit <- na.omit(ValNit[,-1]) ; colnames(ValNit) <- c("EID","LONG","LAT","Nit","Year","Month","GSA")
  ValNPP <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/ValNPP.csv", h=T, dec=".", sep=";",row.names=NULL)  ;  ValNPP <- na.omit(ValNPP[,-1]) ; colnames(ValNPP) <- c("EID","LONG","LAT","NPP","Year","Month","GSA") 
  ValSal <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/ValSal.csv", h=T, dec=".", sep=";",row.names=NULL)  ;  ValSal <- na.omit(ValSal[,-1])  ; colnames(ValSal) <- c("EID","LONG","LAT","Sal","Year","Month","GSA")
  ValTemp <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/ValTemp.csv", h=T, dec=".", sep=";",row.names=NULL);  ValTemp <- na.omit(ValTemp[,-1]) ; colnames(ValTemp) <- c("EID","LONG","LAT","Temp","Year","Month","GSA")
  

                     
  ValChl[ValChl$GSA=="GSA23",'GSA']="GSA22"
  ValDox[ValDox$GSA=="GSA23",'GSA']="GSA22"
  ValNit[ValNit$GSA=="GSA23",'GSA']="GSA22"
  ValNPP[ValNPP$GSA=="GSA23",'GSA']="GSA22"
  ValSal[ValSal$GSA=="GSA23",'GSA']="GSA22"
  ValTemp[ValTemp$GSA=="GSA23",'GSA']="GSA22"

  for(i in 1:6) {
    ValChl[,i] <- as.numeric(as.character(ValChl[,i]))
    ValDox[,i] <- as.numeric(as.character(ValDox[,i]))
    ValNit[,i] <- as.numeric(as.character(ValNit[,i]))
    ValNPP[,i] <- as.numeric(as.character(ValNPP[,i]))
    ValSal[,i] <- as.numeric(as.character(ValSal[,i]))
    ValTemp[,i] <- as.numeric(as.character(ValTemp[,i]))
  }

    unique(ValChl$GSA) 
    unique(ValDox$GSA)
    unique(ValNit$GSA)
    unique(ValNPP$GSA)
    unique(ValSal$GSA)
    unique(ValTemp$GSA)
  
    summary(ValChl) 
    summary(ValDox)
    summary(ValNit)
    summary(ValNPP)
    summary(ValSal)
    summary(ValTemp)


save.image("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data2015.RData")
  

########################## DEBUT SCRIPT POUR MSA GOEFF ##############################################
########################## Janv 2015 ################################################################  
  #fileData =  fileChla(1999-2010)   fileNit(1999-2010)  fileNPP(1999-2010) fileSal(1990-2012)   fileTemp(1990-2012) # c("EID","LONG","LAT","Chl","Year","Month","GSA")  
library(maptools)
gsa.shp <- readShapeSpatial(paste(repertoire_travail, "IFR_F_SUBDIVISION.shp", sep = ""))

par(mfrow=c(3,2))
  plot(ValNit[ValNit$Year==2005,2:3], main="Nitrate", col="green")
  map("world",xlim = range(ValNit$LONG)+c(-1,1), ylim = range(ValNit$LAT)+c(-1,1),add=T,fill=T, col="light grey")
  legend("topright",legend=c("Nitrate","Chla","Temp","Salinity"), pch=19,col=c("green","black","red","blue"),bg="white")
  
  plot(ValSal[ValSal$Year==2005,2:3], main="Salinity", col="blue")
  map("world",xlim = range(ValSal$LONG)+c(-1,1), ylim = range(ValSal$LAT)+c(-1,1),add=T,fill=T, col="light grey")

  plot(ValTemp[ValTemp$Year==2005,2:3], main="Temperature", col="red")
  map("world",xlim = range(ValTemp$LONG)+c(-1,1), ylim = range(ValTemp$LAT)+c(-1,1),add=T,fill=T, col="light grey")

  plot(ValNPP[ValNPP$Year==2005,2:3], main="NPP", col="black")
  map("world",xlim = range(ValNPP$LONG)+c(-1,1), ylim = range(ValNPP$LAT)+c(-1,1),add=T,fill=T, col="light grey")

  plot(ValDox[ValDox$Year==2005,2:3], main="Dox", col="pink")
  map("world",xlim = range(ValDox$LONG)+c(-1,1), ylim = range(ValDox$LAT)+c(-1,1),add=T,fill=T, col="light grey")

  plot(ValChl[ValChl$Year==2005,2:3], main="Chla", col="dark green")
  map("world",xlim = range(ValChl$LONG)+c(-1,1), ylim = range(ValChl$LAT)+c(-1,1),add=T,fill=T, col="light grey")

par(mfrow=c(1,2))
  plot(ValSal[ValSal$Sal!=0,2:3], main="Salinity", col="blue")
  map("world",xlim = range(ValSal$LONG)+c(-1,1), ylim = range(ValSal$LAT)+c(-1,1),add=T,fill=T, col="light grey")

  plot(ValSal[ValSal$Sal==0,2:3], main="Salinity=0", col="red")
  map("world",xlim = range(ValSal$LONG)+c(-1,1), ylim = range(ValSal$LAT)+c(-1,1),add=T,fill=T, col="light grey")

  

#for the MS 2
ValTemps <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/ValTemp.csv", h=T, dec=".", sep=";")
fileTest <- ValTemps[ValTemps$Month==1 & ValTemps$Year==2002,]
write.table(fileTest,"F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/Data_values/fileTest.csv",dec=".",sep=";") 

#write.table(fileTest,"fileTest.csv",sep=";",dec=".")
  plot(fileTest[,2:3], xlab="Longitude" , ylab="Latitude", main="")
  points(ref[,8:7], main="", col="black", pch=19, cex=0.4)
  plot(gsa.shp, xlim = range(fileTest$LONG)+c(-1,1), ylim = range(fileTest$LAT)+c(-1,1), add=TRUE,lwd=1.5)
  map("world",xlim = range(fileTest$LONG)+c(-1,1), ylim = range(fileTest$LAT)+c(-1,1),add=T,fill=T, col="light grey")  
  box()
  text(c(xc+c(2,-1,0,-2,-2,-1,2.5,0,0,0,-0.5,0)),c(yc+c(-0.5,1.5,-1,0,0.5,-1,0.5,0.5,-2.5,-2,-2,-1)),labels=LISTSECTOR,cex=0.65,font=2)
  #arrows(x0=c(6.83),y0=c(39.80),x1=c(7.5),y1=c(39.8))
  text(x=c(7.5,11.5),y=c(39.8,36.5),c("->","->"), font=2)
  #[1] GSA7  GSA8  GSA9  GSA10 GSA11 GSA16 GSA17 GSA18 GSA19 GSA20 GSA22 GSA23

### tests sur correlation inter-variable
  cor(ValSal$Sal, ValNit$Temp)


## carto
 ContourMap <- function(dataFile,year,month,siz=100) {  
  library(akima); library(PBSmapping) ; library(fields)
  x <- dataFile[dataFile$Month==month & dataFile$Year==year,'LONG'] 
  y <- dataFile[dataFile$Month==month & dataFile$Year==year,'LAT'] 
  zval <- dataFile[dataFile$Month==month & dataFile$Year==year,4] 
  data.interp <- interp(x,y,zval,linear = TRUE,xo=seq(min(x), max(x), length = siz),yo=seq(min(y), max(y), length = siz), duplicate="strip")
  image.plot(data.interp,xlab="longitude",ylab="latitude",main=paste(colnames(dataFile)[4], "(",year,"/", month, ")",siz,sep=""), zlim = c(floor(min(dataFile[,4])),ceiling(max(dataFile[,4]))))  #zlim for a common legend to all plots
  #contour(data.interp,add=T, labcex = 0.65, col="gray19")
  map("world",xlim = range(dataFile$LONG)+c(-1,1), ylim = range(dataFile$LAT)+c(-1,1),add=T,fill=T, col="light grey")
  }

 ContourMap.average <- function(dataFile,year,siz=100) {  
  library(akima); library(PBSmapping) ; library(fields)
  x <- dataFile[dataFile$Year==year,'Long'] 
  y <- dataFile[dataFile$Year==year,'Lat'] 
  zval <- dataFile[dataFile$Year==year,4] 
  data.interp <- interp(x,y,zval,linear = TRUE,xo=seq(min(x), max(x), length = siz),yo=seq(min(y), max(y), length = siz), duplicate="strip")
  image.plot(data.interp,xlab="longitude",ylab="latitude",main=paste(colnames(dataFile)[4],"_",year,sep=""), zlim = c(floor(min(dataFile[,4])),ceiling(max(dataFile[,4]))))  #zlim for a common legend to all plots
  #contour(data.interp,add=T, labcex = 0.65, col="gray19")
  map("world",xlim = range(dataFile$Long)+c(-1,1), ylim = range(dataFile$Lat)+c(-1,1),add=T,fill=T, col="light grey")
  }

#for ms2 : average 
  fileTemp_year <- aggregate(x = ValTemp$Temp, by = list(Long=ValTemp$LONG, Lat=ValTemp$LAT, Year=ValTemp$Year), FUN = "mean")  ; colnames(fileTemp_year)[4] <- "TEMP"
  
 for (k in 1990:2012) {
 pdf(paste("Temperature 1990-2012.pdf",sep=""))
  par(mfrow=c(3,4)) # graphique
   for (j in 1990:2012) {
     datTemp <- ContourMap.average(fileTemp_year,j)
     }
  dev.off()
  } # fin boucle pdf  
  
             
 for (k in 1990:2012) {
 pdf(paste("Salinity 1990-2012.pdf",sep=""))
  par(mfrow=c(3,4)) # graphique
   for (j in 1990:2012) {
    for (i in 1:12) {
     datTemp <- ContourMap(ValSal,j,i)
     }
     }
  dev.off()
  } # fin boucle pdf             

 for (k in 1990:2012) {
 pdf(paste("Temperature 1990-2012.pdf",sep=""))
  par(mfrow=c(3,4)) # graphique
   for (j in 1990:2012) {
    for (i in 1:12) {
     datTemp <- ContourMap(ValTemp,j,i)
     }
     }
  dev.off()
  } # fin boucle pdf  

 for (k in 1999:2011) {
 pdf(paste("Nitrate 1999-2010.pdf",sep=""))
  par(mfrow=c(3,4)) # graphique
   for (j in 2001:2010) {
    for (i in 1:12) {
     datTemp <- ContourMap(ValNit,j,i)
     }
     }
  dev.off()
  } # fin boucle pdf             

 for (k in 2001:2010) {
 pdf(paste("NPP 2001-2010.pdf",sep=""))
  par(mfrow=c(3,4)) # graphique
   for (j in 2001:2010) {
    for (i in 1:12) {
     datTemp <- ContourMap(ValNPP,j,i)
     }
     }
  dev.off()
  } # fin boucle pdf             
    
# Fonction pour r�cup�rer les donn�es des fichiers g�n�r�s en haut qui portent par secteur     
 ImportFileSector <- function(fileData, sector) { # sector has to be in "" : "GSA7"  "GSA8"  "GSA9"  "GSA10" "GSA11" "GSA16" "GSA17" "GSA18" "GSA19" "GSA20" "GSA22"
   sect <- read.csv("F:/NANTES_ABA/BDD/Mediterranean_Models/Data_All_Juin/R-ExtractDATA/secteurMed.csv", dec='.',sep=";", row.names=1)
   sect <- data.frame(sect)
   sect$Code_Secteur <- rownames(sect)
   xvil <- as.numeric(sect[sect$Code_Secteur==sector,1:2]) # c(minLONG,maxLONG) cadre geographique pour la Vilaine
   yvil <- as.numeric(sect[sect$Code_Secteur==sector,3:4])
   fVil <- fileData # read.table(paste(fileData,listName,sep="/") , sep = ";", header = T, dec = ",") 
   fileVil <- fVil[fVil$LONG >= xvil[1] & fVil$LONG <= xvil[2] & fVil$LAT <= yvil[2] & fVil$LAT >= yvil[1],]    
   res <- data.frame(fileVil)
   return(res)
  }


  GSAlist <- rownames(sect)
  SectDataSal <- list() 
  SectDataTemp <- list()
  SectDataNit <- list()
  SectDataChla <- list() 
  SectDataNPP <- list()                                                                                                                                                                                                   k
  SectDataDox <- list()                                                                                                                                                                                                      
#list data sector
  for (k in GSAlist){ 
   SectDataSal[[k]] <- ImportFileSector(ValSal,sector=k)
   SectDataTemp[[k]] <- ImportFileSector(ValTemp,sector=k)
   SectDataNit[[k]] <- ImportFileSector(ValNit,sector=k)
   SectDataChla[[k]] <- ImportFileSector(ValChl,sector=k)
   SectDataNPP[[k]] <- ImportFileSector(ValNPP,sector=k)
   SectDataDox[[k]] <- ImportFileSector(ValDox,sector=k)
  }
  str(SectDataChla)


################################################
################################################
### Echelle de variabilit� temporelle d'int�r�t
### Plot Medit globale et par GSA les tendances 
################################################
################################################
# Medit globale
meanEnv <- function(datas,gsa,param="") {
   if (length(datas[,param])==0){
   toto <- 0
   toto2 <- 0   
   res <- data.frame(matrix(NA,nrow=1,ncol=4)) 
   res$Sect = paste(gsa)
   colnames(res) <- c("month","year","Mean","SD","Sect")
   } 
   if (length(datas[,param])>0){
   toto <- aggregate(datas[,param], list(month=datas[,'Month'],year=datas[,'Year']), mean)
   toto2 <- aggregate(datas[,param], list(month=datas[,'Month'],year=datas[,'Year']), sd)
   namSect <- rep(paste(gsa),nrow(toto2))
   res <- cbind(toto,SD=toto2[,3], Sect=namSect)
   colnames(res)[3] <- "Mean"
   }   
 return(res) 
}

### summary 

  AllData.Chla_l <- list() 
  AllData.Sal_l <- list()
  AllData.Temp_l <- list()
  AllData.Nit_l <- list() 
  AllData.Dox_l <- list()                                                                                                                                                                                                   k
  AllData.Npp_l <- list() 

ValSal <- ValSal[ValSal$Sal!=0,]

#list data mean & sd  // selected sampling points
  for (k in GSAlist){ 
   AllData.Chla_l[[k]] <- meanEnv(datas=ValChl[ValChl$GSA==k,],gsa=k,param='Chl')
   AllData.Sal_l[[k]] <- meanEnv(datas=ValSal[ValSal$GSA==k,],gsa=k,param='Sal')  
   AllData.Temp_l[[k]] <- meanEnv(datas=ValTemp[ValTemp$GSA==k,],gsa=k,param='Temp')
   AllData.Nit_l[[k]] <- meanEnv(datas=ValNit[ValNit$GSA==k,],gsa=k,param='Nit')
   AllData.Dox_l[[k]] <- meanEnv(datas=ValDox[ValDox$GSA==k,],gsa=k,param='Dox')
   AllData.Npp_l[[k]] <- meanEnv(datas=ValNPP[ValNPP$GSA==k,],gsa=k,param='NPP') 
  }

#list data mean & sd  // selected areas
  for (k in GSAlist){ 
   AllData.Chla_l2[[k]] <- meanEnv(datas=fileChl[fileChl$GSA==k,],gsa=k,param='chl')
   AllData.Sal_l2[[k]] <- meanEnv(datas=fileSal[fileSal$GSA==k,],gsa=k,param='PSAL')
   AllData.Temp_l2[[k]] <- meanEnv(datas=fileTemp[fileTemp$GSA==k,],gsa=k,param='TEMP')
   AllData.Nit_l2[[k]] <- meanEnv(datas=fileNit[fileNit$GSA==k,],gsa=k,param='nit')
   AllData.Dox_l2[[k]] <- meanEnv(datas=fileDox[fileDox$GSA==k,],gsa=k,param='dox')
   AllData.Npp_l2[[k]] <- meanEnv(datas=fileNPP[fileNPP$GSA==k,],gsa=k,param='npp') 
  }
  
  AllData.Temp <- do.call("rbind",AllData.Temp_l) #; write.table(AllData.Temp,"Temp.csv",sep=";",dec=".")
  AllData.Sal <- do.call("rbind",AllData.Sal_l)   #; write.table(AllData.Sal,"Sal.csv",sep=";",dec=".")
  AllData.Chla <- do.call("rbind",AllData.Chla_l) #; write.table(AllData.Chla,"Chla.csv",sep=";",dec=".")
  AllData.Nit <- do.call("rbind",AllData.Nit_l)   #; write.table(AllData.Nit,"Nit.csv",sep=";",dec=".")
  AllData.NPP <- do.call("rbind",AllData.Npp_l)   #; write.table(AllData.NPP,"NPP.csv",sep=";",dec=".")
  AllData.Dox <- do.call("rbind",AllData.Dox_l)   #; write.table(AllData.Dox,"Dox.csv",sep=";",dec=".")

  AllData.Temp2 <- do.call("rbind",AllData.Temp_l2) #; write.table(AllData.Temp2,"Temp.csv",sep=";",dec=".")
  AllData.Sal2 <- do.call("rbind",AllData.Sal_l2)  #; write.table(AllData.Sal2,"Sal.csv",sep=";",dec=".")
  AllData.Chla2 <- do.call("rbind",AllData.Chla_l2) #; write.table(AllData.Chla2,"Chla.csv",sep=";",dec=".")
  AllData.Nit2 <- do.call("rbind",AllData.Nit_l2)   #; write.table(AllData.Nit2,"Nit.csv",sep=";",dec=".")
  AllData.NPP2 <- do.call("rbind",AllData.Npp_l2)   #; write.table(AllData.NPP2,"NPP.csv",sep=";",dec=".")
  AllData.Dox2 <- do.call("rbind",AllData.Dox_l2)   #; write.table(AllData.Dox2,"Dox.csv",sep=";",dec=".")
                                                                                                             
par(mar=c(2,5,2,2),mfrow=c(3,2)) # environmentam spatial variability (intra-boxplot= intra-zone inter-boxplot= inter-zone
boxplot(AllData.Temp[,'Mean'] ~ AllData.Temp[,'year']*AllData.Temp[,'Sect'],las=2, ylab="", xaxt="n", main="Temperature")
boxplot(AllData.Sal[,'Mean'] ~ AllData.Sal[,'year']*AllData.Sal[,'Sect'],las=2, ylab="", xaxt="n", main="Salinity")
boxplot(AllData.Chla[,'Mean'] ~ AllData.Chla[,'year']*AllData.Chla[,'Sect'],las=2, ylab="", xaxt="n", main="Chla")
boxplot(AllData.Nit[,'Mean'] ~ AllData.Nit[,'year']*AllData.Nit[,'Sect'],las=2, ylab="", xaxt="n", main="Nitrate")
boxplot(AllData.NPP[,'Mean'] ~ AllData.NPP[,'year']*AllData.NPP[,'Sect'],las=2, ylab="", xaxt="n", main="Net Primary Production")
boxplot(AllData.Dox[,'Mean'] ~ AllData.Dox[,'year']*AllData.Dox[,'Sect'],las=2, ylab="", xaxt="n", main="Disolved Oxygen")


#corstarsl
temp.min <- tapply(AllData.Temp[,'Mean'],AllData.Temp[,'Sect'],min)
sal.min <- tapply(AllData.Sal[,'Mean'],AllData.Sal[,'Sect'],min)
dox.min <- tapply(AllData.Dox[,'Mean'],AllData.Dox[,'Sect'],min)
nit.min <- tapply(AllData.Nit[,'Mean'],AllData.Nit[,'Sect'],min)
Chla.min <- tapply(AllData.Chla[,'Mean'],AllData.Chla[,'Sect'],min)
NPP.min <- tapply(AllData.NPP[,'Mean'],AllData.NPP[,'Sect'],min)

temp.max <- tapply(AllData.Temp[,'SD'],AllData.Temp[,'Sect'],max)
sal.max <- tapply(AllData.Sal[,'SD'],AllData.Sal[,'Sect'],max)
dox.max <- tapply(AllData.Dox[,'SD'],AllData.Dox[,'Sect'],max)
nit.max <- tapply(AllData.Nit[,'SD'],AllData.Nit[,'Sect'],max)
Chla.max <- tapply(AllData.Chla[,'SD'],AllData.Chla[,'Sect'],max)
NPP.max <- tapply(AllData.NPP[,'SD'],AllData.NPP[,'Sect'],max)


temp.mean <- tapply(AllData.Temp[,'Mean'],AllData.Temp[,'Sect'],mean)
sal.mean <- tapply(AllData.Sal[,'Mean'],AllData.Sal[,'Sect'],mean)
dox.mean <- tapply(AllData.Dox[,'Mean'],AllData.Dox[,'Sect'],mean)
nit.mean <- tapply(AllData.Nit[,'Mean'],AllData.Nit[,'Sect'],mean)
Chla.mean <- tapply(AllData.Chla[,'Mean'],AllData.Chla[,'Sect'],mean)
NPP.mean <- tapply(AllData.NPP[,'Mean'],AllData.NPP[,'Sect'],mean)

temp.sd <- tapply(AllData.Temp[,'Mean'],AllData.Temp[,'Sect'],sd)
sal.sd <- tapply(AllData.Sal[,'Mean'],AllData.Sal[,'Sect'],sd)
dox.sd <- tapply(AllData.Dox[,'Mean'],AllData.Dox[,'Sect'],sd)
nit.sd <- tapply(AllData.Nit[,'Mean'],AllData.Nit[,'Sect'],sd)
Chla.sd <- tapply(AllData.Chla[,'Mean'],AllData.Chla[,'Sect'],sd)
NPP.sd <- tapply(AllData.NPP[,'Mean'],AllData.NPP[,'Sect'],sd)

temp.coefi = list()
sal.coefi = list()
dox.coefi = list()
nit.coefi = list()
Chla.coefi = list()
NPP.coefi = list()

                               #  summary(lm(tapply(AllData.Sal[,'Mean'],list(AllData.Sal[,'year'],AllData.Sal[,'Sect']),mean)[,i] ~ as.numeric(unique(AllData.Sal$year))))$coefficient[2,1]
for (i in 1:12) {              # anova(lm(tapply(AllData.Temp[,'Mean'],list(AllData.Temp[,'year'],AllData.Temp[,'Sect']),mean)[,i] ~ as.numeric(unique(AllData.Temp$year))))[1,5]
temp.coefi[[i]] <- coef( summary(lm(tapply(AllData.Temp[,'Mean'],list(AllData.Temp[,'year'],AllData.Temp[,'Sect']),mean)[,i] ~ as.numeric(unique(AllData.Temp$year)))))[2]
sal.coefi[[i]] <- coef( summary(lm(tapply(AllData.Sal[,'Mean'],list(AllData.Sal[,'year'],AllData.Sal[,'Sect']),mean)[,i] ~ as.numeric(unique(AllData.Sal$year)))))[2]
dox.coefi[[i]] <- coef( summary(lm(tapply(AllData.Dox[,'Mean'],list(AllData.Dox[,'year'],AllData.Dox[,'Sect']),mean)[,i] ~ as.numeric(unique(AllData.Dox$year)))))[2]
nit.coefi[[i]] <- coef( summary(lm(tapply(AllData.Nit[,'Mean'],list(AllData.Nit[,'year'],AllData.Nit[,'Sect']),mean)[,i] ~ as.numeric(unique(AllData.Nit$year)))))[2]
Chla.coefi[[i]] <- coef( summary(lm(tapply(AllData.Chla[,'Mean'],list(AllData.Chla[,'year'],AllData.Chla[,'Sect']),mean)[,i] ~ as.numeric(unique(AllData.Chla$year)))))[2]
NPP.coefi[[i]] <- coef( summary(lm(tapply(AllData.NPP[,'Mean'],list(AllData.NPP[,'year'],AllData.NPP[,'Sect']),mean)[,i] ~ as.numeric(unique(AllData.NPP$year)))))[2]
}

temp.coef = do.call("cbind",temp.coefi); colnames(temp.coef) <- unique(AllData.Temp[,'Sect']) ; rownames(temp.coef) <- "temp.coef"
sal.coef = do.call("cbind",sal.coefi); colnames(sal.coef) <- unique(AllData.Sal[,'Sect']) ; rownames(sal.coef) <- "sal.coef"
dox.coef = do.call("cbind",dox.coefi); colnames(dox.coef) <- unique(AllData.Dox[,'Sect']) ; rownames(dox.coef) <- "dox.coef"
nit.coef = do.call("cbind",nit.coefi); colnames(nit.coef) <- unique(AllData.Nit[,'Sect']) ; rownames(nit.coef) <- "nit.coef"
Chla.coef = do.call("cbind",Chla.coefi); colnames(Chla.coef) <- unique(AllData.Chla[,'Sect']) ; rownames(Chla.coef) <- "Chla.coef"
NPP.coef = do.call("cbind",NPP.coefi); colnames(NPP.coef) <- unique(AllData.NPP[,'Sect']) ; rownames(NPP.coef) <- "NPP.coef"

tab.env=(round(rbind(temp.coef,sal.coef,dox.coef,nit.coef,Chla.coef,NPP.coef,env.gsa2[32,]),5))
fix(tab.env)
env <- rbind(temp.mean,temp.min,temp.max,temp.sd,temp.coef,
             sal.mean,sal.min,sal.max,sal.sd,sal.coef,
             dox.mean,dox.min,dox.max,dox.sd,dox.coef,
             nit.mean,nit.min,nit.max,nit.sd,nit.coef,
             Chla.mean,Chla.min,Chla.max,Chla.sd,Chla.coef,
             NPP.mean,NPP.min,NPP.max,NPP.sd,NPP.coef)
write.table(env,"F:/NANTES_ABA/PROJETS/PHOTOFISH2011/R/env.csv", sep=";",dec=".")
env.gsa2=rbind(env,env.gsa[19:20,])
write.table(env.gsa2,"F:/NANTES_ABA/PROJETS/PHOTOFISH2011/R/env_GSA.csv", sep=";",dec=".")
env.gsa=read.csv("F:/NANTES_ABA/PROJETS/PHOTOFISH2011/R/env_GSA.csv",sep=";",dec=".") 
fao.gsa=read.csv("F:/NANTES_ABA/PROJETS/PHOTOFISH2011/R/FAO.ok_2014.csv",sep=";",dec=".") 
    

par(mfrow=c(3,2)) # environmentam spatial variability (intra-boxplot= intra-zone inter-boxplot= inter-zone
boxplot(AllData.Temp[,'Mean'] ~ AllData.Temp[,'year']*AllData.Temp[,'Sect'],las=2, ylab="Temperature", xaxt="n", main="Temperature")
boxplot(AllData.Sal[,'Mean'] ~ AllData.Sal[,'year']*AllData.Sal[,'Sect'],las=2, ylab="Salinity", xaxt="n", main="Salinity")
boxplot(AllData.Chla[,'Mean'] ~ AllData.Chla[,'year']*AllData.Chla[,'Sect'],las=2, ylab="Chla", xaxt="n", main="Chla")
boxplot(AllData.Nit[,'Mean'] ~ AllData.Nit[,'year']*AllData.Nit[,'Sect'],las=2, ylab="Nitrate", xaxt="n", main="Nitrate")
boxplot(AllData.NPP[,'Mean'] ~ AllData.NPP[,'year']*AllData.NPP[,'Sect'],las=2, ylab="Net Primary Production", xaxt="n", main="Net Primary Production")
boxplot(AllData.Dox[,'Mean'] ~ AllData.Dox[,'year']*AllData.Dox[,'Sect'],las=2, ylab="Disolved Oxygen", xaxt="n", main="Disolved Oxygen")




## Depth
depth <- read.table("F:/NANTES_ABA/BDD/Mediterranean_Models/depth/C3xyz.txt", sep=";", dec=".", h=T)  
xlim = range(ValChl$LONG)+c(-1,1)
ylim = range(ValChl$LAT)+c(-1,1)
library(akima); library(PBSmapping) ; library(fields) ; require(ggplot2)
  depth2 <- unique(depth[,-4])
  x <- depth[,1] 
  y <- depth[,2] 
  zval <- depth[,3] 
  qplot(x,y,colour= zval)
  
  data.interp <- interp(x,y,zval,linear = TRUE,xo=seq(min(x), max(x), length = 1),yo=seq(min(y), max(y), length = 1), duplicate="strip")
  image.plot(data.interp,xlab="longitude",ylab="latitude",main="average depth" )  #zlim for a common legend to all plots
  #contour(data.interp,add=T, labcex = 0.65, col="gray19")
   map("world",xlim = range(ValChl$LONG)+c(-1,1), ylim = range(ValChl$LAT)+c(-1,1),add=T,fill=T, col="light grey")




# Medit par GSA / moyenne annuelle
print("GSA07")
 mean07.chla=aggregate(SectDataChla$GSA7[,'chl'], list(month=SectDataChla$GSA7[,'Month'],year=SectDataChla$GSA7[,'Year']), mean)
   paste("m=",round(summary(lm(mean07.chla$x~mean07.chla$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean07.chla$x~mean07.chla$year))[,5][1],3),sep="") 
 mean07.nit=aggregate(SectDataNit$GSA7[,'nit'], list(month=SectDataNit$GSA7[,'Month'],year=SectDataNit$GSA7[,'Year']), mean)
   paste("m=",round(summary(lm(mean07.nit$x~mean07.nit$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean07.nit$x~mean07.nit$year))[,5][1],3),sep="")
 mean07.npp=aggregate(SectDataNpp$GSA7[,'npp'], list(month=SectDataNpp$GSA7[,'Month'],year=SectDataNpp$GSA7[,'Year']), mean)
   paste("m=",round(summary(lm(mean07.npp$x~mean07.npp$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean07.npp$x~mean07.npp$year))[,5][1],3),sep="")   
 mean07.temp=aggregate(SectDataTemp$GSA7[,'TEMP'], list(month=SectDataTemp$GSA7[,'Month'],year=SectDataTemp$GSA7[,'Year']), mean)
    paste("m=",round(summary(lm(mean07.temp$x~mean07.temp$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean07.temp$x~mean07.temp$year))[,5][1],3),sep="") 
 mean07.sal=aggregate(SectDataSal$GSA7[,'PSAL'], list(month=SectDataSal$GSA7[,'Month'],year=SectDataSal$GSA7[,'Year']), mean)
   paste("m=",round(summary(lm(mean07.sal$x~mean07.sal$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean07.sal$x~mean07.sal$year))[,5][1],3),sep="") 

print("GSA19")
 mean19.chla=aggregate(SectDataChla$GSA19[,'chl'], list(month=SectDataChla$GSA19[,'Month'],year=SectDataChla$GSA19[,'Year']), mean)
   paste("m=",round(summary(lm(mean19.chla$x~mean19.chla$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean19.chla$x~mean19.chla$year))[,5][1],3),sep="")
 mean19.nit=aggregate(SectDataNit$GSA19[,'nit'], list(month=SectDataNit$GSA19[,'Month'],year=SectDataNit$GSA19[,'Year']), mean)
   paste("m=",round(summary(lm(mean19.nit$x~mean19.nit$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean19.nit$x~mean19.nit$year))[,5][1],3),sep="")
 mean19.temp=aggregate(SectDataTemp$GSA19[,'TEMP'], list(month=SectDataTemp$GSA19[,'Month'],year=SectDataTemp$GSA19[,'Year']), mean)
  paste("m=",round(summary(lm(mean19.temp$x~mean19.temp$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean19.temp$x~mean19.temp$year))[,5][1],3),sep="")
 mean19.sal=aggregate(SectDataSal$GSA19[,'PSAL'], list(month=SectDataSal$GSA19[,'Month'],year=SectDataSal$GSA19[,'Year']), mean)
   paste("m=",round(summary(lm(mean19.sal$x~mean19.sal$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean19.sal$x~mean19.sal$year))[,5][1],3),sep="")
 mean19.npp=aggregate(SectDataNpp$GSA19[,'npp'], list(month=SectDataNpp$GSA19[,'Month'],year=SectDataNpp$GSA19[,'Year']), mean)
   paste("m=",round(summary(lm(mean19.npp$x~mean19.npp$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean19.npp$x~mean19.npp$year))[,5][1],3),sep="")   

 print("GSA22")
 mean22.chla=aggregate(SectDataChla$GSA22[,'chl'], list(month=SectDataChla$GSA22[,'Month'],year=SectDataChla$GSA22[,'Year']), mean)
   paste("m=",round(summary(lm(mean22.chla$x~mean22.chla$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean22.chla$x~mean22.chla$year))[,5][1],3),sep="")
 mean22.nit=aggregate(SectDataNit$GSA22[,'nit'], list(month=SectDataNit$GSA22[,'Month'],year=SectDataNit$GSA22[,'Year']), mean)
   paste("m=",round(summary(lm(mean22.nit$x~mean22.nit$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean22.nit$x~mean22.nit$year))[,5][1],3),sep="")
 mean22.temp=aggregate(SectDataTemp$GSA22[,'TEMP'], list(month=SectDataTemp$GSA22[,'Month'],year=SectDataTemp$GSA22[,'Year']), mean)
   paste("m=",round(summary(lm(mean22.temp$x~mean22.temp$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean22.temp$x~mean22.temp$year))[,5][1],3),sep="")
 mean22.sal=aggregate(SectDataSal$GSA22[,'PSAL'], list(month=SectDataSal$GSA22[,'Month'],year=SectDataSal$GSA22[,'Year']), mean)
   paste("m=",round(summary(lm( mean22.sal$x~ mean22.sal$year))$coefficients[,1][2],3),", pval=",round(anova(lm( mean22.sal$x~mean22.sal$year))[,5][1],3),sep="")
 mean22.npp=aggregate(SectDataNpp$GSA22[,'npp'], list(month=SectDataNpp$GSA22[,'Month'],year=SectDataNpp$GSA22[,'Year']), mean)
   paste("m=",round(summary(lm(mean22.npp$x~mean22.npp$year))$coefficients[,1][2],3),", pval=",round(anova(lm(mean22.npp$x~mean22.npp$year))[,5][1],3),sep="")   
 
par(mfrow=c(5,3))
 plot(mean07.nit$year,mean07.nit$x, lwd=2, type='l', pch=1, main="GSA7",ylab="Nitrate",xlab=""); plot(mean19.nit$year,mean19.nit$x, lwd=2, pch=2, ylab="",xlab="",type="l", main="GSA19"); 
 legend("topright",legend="Nitrate",bty="n", cex=1.2) ; plot(mean22.nit$year,mean22.nit$x, lwd=2, type="l", main="GSA22",ylab="",xlab="")
 
 plot(mean07.chla$year,mean07.chla$x, lwd=2, type='l',ylab="Chla",xlab=""); plot(mean19.chla$year,mean19.chla$x, lwd=2,type='l' ,ylab="",xlab=""); legend("topright",legend="Chla",bty="n", cex=1.2)
 plot(mean22.chla$year,mean22.chla$x, lwd=2, type='l',ylab="",xlab="")                  
 
 
 plot(mean07.temp$year,mean07.temp$x, lwd=2, type='l',ylab="Temperature"); plot(mean19.temp$year,mean19.temp$x, type='l',lwd=2,ylab="",xlab=""); legend("bottomright",legend="Temperature",bty="n", cex=1.2)
 plot(mean22.temp$year,mean22.temp$x, lwd=2, type='l',ylab="")
 
 
 plot(mean07.sal$year,mean07.sal$x, lwd=2, type='l',ylab="Salinity",xlab=""); plot(mean19.sal$year,mean19.sal$x, type='l',lwd=2,ylab="",xlab=""); legend("bottomright",legend="Salinity",bty="n", cex=1.2)
 plot(mean22.sal$year,mean22.sal$x, lwd=2, type='l',ylab="",xlab="")
 
 plot(mean07.npp$year,mean07.npp$x, lwd=2, type='l',ylab="NPP",xlab=""); plot(mean19.npp$year,mean19.npp$x, type='l',lwd=2,ylab="",xlab=""); legend("bottomright",legend="NPP",bty="n", cex=1.2)
 plot(mean22.npp$year,mean22.npp$x, lwd=2, type='l',ylab="",xlab="")

 boxplot(SectDataNpp$GSA22[,'npp']~SectDataNpp$GSA22[,'Year'], main="NPP")

#mediterranean basin
par(mfrow=c(2,2))   
   nit.all=aggregate(fileNit[,'nit'], list(month=fileNit[,'Month'],year=fileNit[,'Year']), mean)
   plot(nit.all$year,nit.all$x, lwd=2, type='l',ylab="Nitrate",xlab="",main="Mediterranean basin")
   
   chla.all=aggregate(fileChl[,'chl'], list(month=fileChl[,'Month'],year=fileChl[,'Year']), mean)
   plot(chla.all$year,chla.all$x, lwd=2, type='l',ylab="Chla",xlab="")
   
   temp.all=aggregate(fileTemp[,'TEMP'], list(month=fileTemp[,'Month'],year=fileTemp[,'Year']), mean)
   plot(temp.all$year,temp.all$x, lwd=2, type='l',ylab="Temperature",xlab="")
   
   sal.all=aggregate(fileSal[,'PSAL'], list(month=fileSal[,'Month'],year=fileSal[,'Year']), mean)
   plot(sal.all$year,sal.all$x, lwd=2, type='l',ylab="Salinity",xlab="")


