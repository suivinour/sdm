# ---------------------------------------------------------------------------- #
#           SUIVI NOUR    Mise a jour de la distribution des nourricerie                 
# ---------------------------------------------------------------------------- #
# 05d. Transform chl and nppv from a daily scale to a April - Septembre scale.
# ---------------------------------------------------------------------------- #

### Calling all of the common object to my analyses
source(paste0("./scripts/boot.R"))

############# Chl and Nppv
Cop_product <- c(
  "Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_chl",
  "Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_nppv")

### Load the path for each raster layer ----------------------------------------

for (prod_i in Cop_product) {
  
  print(paste0(prod_i, " --- initiated"))
  
  path_to_data_product_daily <- paste0(path_to_data_raw,
                                    "Env_data/Copernicus/",
                                    prod_i)
  
  ## Create date vector --------------------------------------------------------
  
  list_nc <- list.files(path_to_data_product_daily)
  list_nc <- grep(pattern = '_04_|_05_|_06_|_07_|_08_|_09_', list_nc, value = TRUE)
  
  
  list_date <- str_remove_all(str_sub(list_nc,-13),
                              paste(c(".nc"),
                                    collapse = "|"))
  
  list_date<- str_replace_all(list_date,
                              "_","-")
  
  year_list<-format(as.Date(list_date), "%Y")
  year_list

 
  ###Initialize parallelisation
  n.cores <- detectCores()  # My computer have 8 cores
  clust <- makeCluster(n.cores - 6) ## I will use only 4 cores
  registerDoParallel(clust)
  foreach::getDoParWorkers()
  
  
  clusterEvalQ(clust, {
    library("raster")
    library("stringr")
    
  })
  
  ## Create output folder -----------------------------------------------------
  folder_output_mean <- paste0(path_to_data_tidy,
                          "Env_data/",
                          str_replace(prod_i, "_daily", "") ,
                          "_mean_May_August")
  
  dir.create(folder_output_mean,
             recursive = T,
             showWarnings = F)
  
  
  ### Load each raster stack and compute the mean
  foreach (year_i = 1 : length(unique(year_list)), .verbose = TRUE)%dopar%{
    
    nc_year<-list_nc[which(year_list==unique(year_list)[year_i])]
    
    day_stack <- stack(paste0(path_to_data_product_daily, "/", nc_year))
    
    
    # Compute the mean ---------------------------------------------------------
    
    year_mean <- mean(day_stack, na.rm=T)
    
    
    names_layer<-str_replace(prod_i, "_daily", "")
    names_layer<-paste0(names_layer,
                        "_mean_",
                        str_replace_all(unique(year_list)[year_i],"-","_"), 
                        '.grd')
    
    # Save Raster 
    writeRaster(year_mean, paste0(folder_output_mean, "/", names_layer),overwrite = FALSE)
    
    
  }
  stopCluster(clust)
  
  
  
  
}

