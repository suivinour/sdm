# ---------------------------------------------------------------------------- #
#           SUIVI NOUR    Mise a jour de la distribution des nourricerie                 
# ---------------------------------------------------------------------------- #
# 02. Transform wave parameter from a daily scale to a monthly scale (mean).
# ---------------------------------------------------------------------------- #

### Calling all of the common object to my analyses
source(paste0("./scripts/boot.R"))

############# Salinity and oxygen
Cop_product <- c(
  "Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_so_bottom_daily")

#Load the path for each raster layer

for (prod_i in Cop_product) {

  print(paste0(prod_i, " --- initiated"))
  
  path_to_data_tidy_daily <- paste0(path_to_data_tidy,
                                         "Env_data/",
                                         prod_i)
  
  
  ## Create date vector
  list_nc <- list.files(path_to_data_tidy_daily)
  list_nc <-grep( ".grd", list_nc , value = TRUE)  
  
  list_date <- str_remove_all(str_sub(list_nc,-14),
                          paste(c(".grd"),
                                collapse = "|"))
  list_date<- str_replace_all(list_date,
                             "_","-")
  
  month_list<-format(as.Date(list_date), "%Y-%m")
  month_list
    
###Initialize parallelisation
n.cores <- detectCores()  # My computer have 8 cores
clust <- makeCluster(n.cores - 4) ## I will use only 4 cores
registerDoParallel(clust)
foreach::getDoParWorkers()


clusterEvalQ(clust, {
  library("raster")
  library("stringr")
  
})

folder_output <- paste0(path_to_data_tidy,
                        "Env_data/",
                        str_replace(prod_i, "_daily", "") ,
                        "_monthly")

dir.create(folder_output,
           recursive = T,
           showWarnings = F)

### Load each raster stack and compute the mean 
foreach (month_i = 1 : length(unique(month_list)), .verbose = TRUE)%dopar%{

nc_month<-list_nc[which(month_list==unique(month_list)[month_i])]
  
day_stack <- stack(paste0(  path_to_data_tidy_daily, "/", nc_month))

monthly_mean <- mean(day_stack, na.rm=T)
monthly_cv <- mean(day_stack, na.rm=T)



names_layer<-str_replace(prod_i, "_daily", "")
names_layer<-paste0(names_layer,
                    "_",
                    str_replace_all(unique(month_list)[month_i],"-","_"), 
                    '.grd')

# Save Raster 
writeRaster(monthly_mean, paste0(folder_output, "/", names_layer),overwrite = FALSE)

}
stopCluster(clust)
}


#### bottom temperature, nppv, chla

Cop_product <- c(
  "Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_bottomT",
  "Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_nppv", 
  "Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_chl")

#Load the path for each raster layer

for (prod_i in Cop_product) {
  
  print(paste0(prod_i, " --- initiated"))
  
  path_to_data_tidy_daily <- paste0(path_to_data_tidy,
                                    "Env_data/",
                                    prod_i)
  
  
  ## Create date vector
  list_nc <- list.files(path_to_data_tidy_daily)
  list_nc <-grep( ".grd", list_nc , value = TRUE)  
  
  list_date <- str_remove_all(str_sub(list_nc,-14),
                              paste(c(".grd"),
                                    collapse = "|"))
  list_date<- str_replace_all(list_date,
                              "_","-")
  
  month_list<-format(as.Date(list_date), "%Y-%m")
  month_list
  
  ###Initialize parallelisation
  n.cores <- detectCores()  # My computer have 8 cores
  clust <- makeCluster(n.cores - 2) 
  registerDoParallel(clust)
  foreach::getDoParWorkers()
  
  
  clusterEvalQ(clust, {
    library("raster")
  })
  
  folder_output <- paste0(path_to_data_tidy,
                          "Env_data/",
                          str_replace(prod_i, "_daily", "") ,
                          "_monthly")
  
  dir.create(folder_output,
             recursive = T,
             showWarnings = F)
  
  ### Load each raster stack and compute the mean 
  foreach (month_i = 1 : length(unique(month_list)), .verbose = TRUE)%dopar%{
    
    nc_month<-list_nc[which(month_list==unique(month_list)[month_i])]
    
    day_stack <- stack(paste0(  path_to_data_tidy_daily, "/", nc_month))
    
    monthly_mean <- mean(day_stack)
    
    
    
    names_layer<-str_replace(prod_i, "_daily", "")
    names_layer<-paste0(names_layer,
                        "_",
                        str_replace_all(unique(month_list)[month_i],"-","_"), 
                        '.grd')
    
    # Save Raster 
    writeRaster(monthly_mean, paste0(folder_output, "/", names_layer),overwrite = FALSE)
    
  }
  stopCluster(clust)
}