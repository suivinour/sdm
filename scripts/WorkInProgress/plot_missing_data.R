#################### SUIVINOUR ################################################
###############################################################################

###This script is here to track the missing values after joining occurences and 
### env data


### Calling all of the common object to my analyses
source("./scripts/boot.R")

### The points before 09/1993 don't have value as the 1992 climatic data were not 
### available

env_i <- c("chl")

Chl <- stack(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/",env_i,"_mean_April_Sept.grd"))

## Take randomly 40 points in the raster stack
sample_random <- sampleRandom(Chl,size = 100)

sample_random <- sample_random %>% 
  as.data.frame(sample_random) 

sample <- gather(sample_random,"Year", "Chl", 1 : 30)

sample$Year <- as.numeric(str_sub(sample$Year, start = 2))



ggplot(data = sample, mapping = aes(x=Year, y= Chl))+
  geom_point(size = 0.5)+
  geom_smooth(colour = "red")+ #Method GAM utilise
  ggtitle("Evolution de la Chla sur la période 1993 - 2021   n = 100 points" ) 

########## Check with nppv

env_i <- c("nppv")

Nppv <- stack(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/",env_i,"_mean_April_Sept.grd"))

## Take randomly 40 points in the raster stack
sample_random_n <- sampleRandom(Nppv,size = 100)

sample_random_n <- sample_random_n %>% 
  as.data.frame(sample_random_n) 

sample_n <- gather(sample_random_n,"Year", "Nppv", 1 : 30)

sample_n$Year <- as.numeric(str_sub(sample_n$Year, start = 2))



ggplot(data = sample_n, mapping = aes(x=Year, y= Nppv))+
  geom_point(size = 0.5)+
  geom_smooth(colour = "red")+ #Method GAM utilise
  ggtitle("Evolution de la NPP sur la période 1993 - 2021   n = 100 points" ) 






Occ_Solea <- read_delim(paste0(path_to_data_tidy,'Occ/Regional/',"Solea solea","_Regional_scale_occurences_env.csv"),delim = ",", col_select = c(2:17))
  
Occ_Merlan <- read_delim(paste0(path_to_data_tidy,'Occ/Regional/',"Merlangius merlangus","_Regional_scale_occurences_env.csv"),delim = ",", col_select = c(2:17))

# Remove 1993.01
Occ_Solea <- Occ_Solea %>% 
  filter (YM != 1993.01)

Occ_Solea_temp <- Occ_Solea %>% 
  filter (YM %in% c(1993.02:1993.08)) %>% 
  mutate(Missing = "Missing")

Occ_Solea_t <- Occ_Solea %>% 
  filter (YM != c(1993.02:1993.08)) %>% 
  filter(is.na(o2_bottom)==FALSE) %>%
  filter(is.na(so_bottom)==FALSE) %>% 
  mutate(Missing = "Present") %>% 
  rbind(Occ_Solea_temp)


library(FactoMineR)

Occ_Solea_PCA <- Occ_Solea_t %>% 
  dplyr::select(o2_bottom,so_bottom,bottomT,NAO,Missing)

ACP <- PCA(Occ_Solea_PCA, quali.sup = 5)


plot(ACP, 
     axes = c(1, 2),
     habillage = 5, 
     col.hab = c("black","red"), 
     title = "Dataset projected onto PC1-2 Subspace",
     label="none")

plot(ACP, 
     axes = c(3, 4),
     habillage = 5, 
     col.hab = c("black","red"), 
     title = "Dataset projected onto PC3-4 Subspace",
     label="none")

######### Merlan #####################################################

Occ_Merlan <- read_delim(paste0(path_to_data_tidy,'Occ/Regional/',"Merlangius merlangus","_Regional_scale_occurences_env.csv"),delim = ",", col_select = c(2:17))

# Remove 1993.01
Occ_Merlan <- Occ_Merlan %>% 
  filter (YM != 1993.01)

Occ_Merlan_temp <- Occ_Merlan %>% 
  filter (YM %in% c(1993.02:1993.08)) %>% 
  mutate(Missing = "Missing")

Occ_Merlan_t <- Occ_Merlan %>% 
  filter (YM != c(1993.02:1993.08)) %>% 
  filter(is.na(o2_bottom)==FALSE) %>%
  filter(is.na(so_bottom)==FALSE) %>% 
  mutate(Missing = "Present") %>% 
  rbind(Occ_Merlan_temp)


library(FactoMineR)

Occ_Merlan_PCA <- Occ_Merlan_t %>% 
  dplyr::select(o2_bottom,so_bottom,bottomT,NAO,Missing)

ACP <- PCA(Occ_Merlan_PCA, quali.sup = 5)


plot(ACP, 
     axes = c(1, 2),
     habillage = 5, 
     col.hab = c("black","red"), 
     title = "M. merlangus projected onto PC1-2 Subspace",
     label="none")

plot(ACP, 
     axes = c(3, 4),
     habillage = 5, 
     col.hab = c("black","red"), 
     title = "M. merlangus projected onto PC3-4 Subspace",
     label="none")



################################################################################
################################################################################

# The salinity data is weird in 2020 (value = -0.001)

path_to_product<- paste0(path_to_data_tidy,
                         "Env_data/Copernicus_interpolated/Copernicus_GLOBAL_MULTIYEAR_PHY_001_030-TDS_so_bottom/")
path_to_product <-paste0(path_to_product,list.files(path_to_product,pattern = ".gri"))

path_to_product_forecast <-paste0(path_to_data_tidy,
                                  "Env_data/Copernicus_interpolated/Copernicus_GLOBAL_ANALYSISFORECAST_PHY_001_024-TDS_so_bottom/")
path_to_product_forecast <-paste0(path_to_product_forecast,list.files(path_to_product_forecast,pattern = ".gri"))

path_to_product <- c(path_to_product,path_to_product_forecast)

# Check 2020.06
raster_temp <- raster(path_to_product[329])

plot(raster_temp)
