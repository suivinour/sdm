# ---------------------------------------------------------------------------- #
#           SUIVI NOUR    Mise a jour de la distribution des nourricerie                 
# ---------------------------------------------------------------------------- #
# 06. Prepartion of the data for species disrtibution models
# ---------------------------------------------------------------------------- #

### This script will check the multicollinearity between each variables
## in the Local scale model dataset

### Calling all of the common object to my analyses
source("./scripts/boot.R")

## Select the species
sp = 1

### Import Occurences
Occurences <-  read_delim(paste0(path_to_data_tidy,"Occ/Local/", species[sp],"_Local_scale_occurences_env.csv"),delim = ",", col_select = c(2:26))

### There is two ways to check the potential colinearity among variables, 
### Pearson's correlation matrix and VIF
## Threshold for Pearson correlation is 0.7
## Threshold for VIF is around 10


## Pearson -------------------------------------------------------------------
Correlation_df <- Occurences %>% 
  dplyr::select(bathymetry,chl_mean : sali_perc_90)

## Compute pearson correlation 
correlation <- as.data.frame(correlation(Correlation_df, method = "pearson"))
write.csv(correlation, paste0(path_to_data_tidy,"Occ/Local/", species[sp],"_pearson_correlation.csv"))

## Get plot of correlations
# png(paste0(path_to_figures,"Local scale occurences/",species[sp],"_correlation.png"),
#     width = 1100,height = 700, units = "px")
# ecospat.cor.plot(Correlation_df)
# dev.off()

#### We  decide to remove : 
## NPP / Temp_mean / Salinity cv, treshold_10 and perc_90
## 
### I remove oxygen as this variable wasn't ecologically useful (values > 90% saturation)
##
### I choose to keep bathymetry as it's related with other process than temperature

Occurences_select <- Occurences %>% 
  dplyr::select(- c(nppv_mean,o2_mean,o2_cv,o2_perc_25,temp_mean,sali_cv,sali_tresh_10,sali_perc_90))

## Pearson
Correlation_df <- Occurences_select %>% 
  dplyr::select(bathymetry,chl_mean : sali_mean)

## Check if there is still correlation------------------------------------------
correlation(Correlation_df, method = "pearson")


## Plot correlation
#ecospat.cor.plot(Correlation_df)

## Test with VIF to make sure that there is no hidden correlation

VIF <- vif(as.data.frame(Correlation_df))
write.csv(VIF, paste0(path_to_figures,"Local scale occurences/", species[sp],"_VIF.csv"))
# Every variable is below 10 so it's good

## Plot VIF
# ggplot(data = VIF, mapping = aes(x = Variables, y = VIF))+
#   geom_histogram(stat = "identity")+
#   theme_bw()+
#   ggtitle(paste0(species[sp], " VIF test")) +
#   theme(plot.title = element_text(hjust = 0.5))
# ggsave(paste0(species[sp], " VIF test",".tiff"),width = 7, height = 5, units = "in",path = paste0(path_to_figures,"Local scale occurences/"))


# Save file
write.csv(Occurences_select, file = paste0(path_to_data_tidy,"Occ/Local/",species[sp],"_Local_scale_model_data.csv"))


################################################################################
################################################################################

