# ---------------------------------------------------------------------------- #
#           SUIVI NOUR    Mise a jour de la distribution des nourricerie                 
# ---------------------------------------------------------------------------- #
# 01. Projection of Solea Solea Regional distribution
# ---------------------------------------------------------------------------- #

## In this script We will compare the projection between different models

### Calling all of the common object to my analyses
source("./scripts/boot.R")

# Start with Solea
sp = 1

### Import Occurences
Occurences <-  read_delim(paste0(path_to_data_tidy,"Occ/Regional/", species[sp],"_Regional_scale_model_data.csv"),delim = ",", col_select = c(2:18))

## Start with total ensemble model of Solea solea

models <- get(load("./Regional.total.model/Regional.total.model.Regional.total.model_Solea solea.models.out"))

ensemble_model <- model <- get(load("./Regional.total.model/Regional.total.model.Regional.total.model_Solea solea.ensemble.models.out"))



path_to_environment <-paste0(path_to_data_tidy,"Env_data/Projection/",list.files(paste0(path_to_data_tidy,"Env_data/Projection/"),pattern = ".gri"))

## create date information for names 

Annee <- c(1993:2021)
Mois <- c("01","02","03","04","05","06","07","08","09","10","11","12")
YM_vector <- c()
prediction_df <- c()
prediction_stack <- stack()
mess <- stack()
#Creation of a date vector
for (y in c(1 : length(Annee))){
  for (m in c(1 : length(Mois))){
    name <- c(paste0(Annee[y],".",Mois[m]))
    YM_vector <- c(YM_vector,name)
  }
}
# 1993_01 is not present in our data
YM_vector <- YM_vector[2:348]

###################### Projection and uncertainty ##############################
################################################################################

###Initialize parallelisation
n.cores <- detectCores()  # My computer have 8 cores
clust <- makeCluster(n.cores - 6) ## I will use only 2 cores
registerDoParallel(clust)
foreach::getDoParWorkers()


clusterEvalQ(clust, {
  library("raster")
  library("biomod2")
  library("dismo")
})

# Add foreach loop
current_env <- stack(path_to_environment[1])

names(current_env)<- c("so_bottom","bottomT","o2_bottom","bathymetry","substrat","NAO","chl")

################################# Projection #################################

### Project each individual model

Solea_model_proj_current <- BIOMOD_Projection(
                            bm.mod = models,
                            new.env = current_env,
                            proj.name = "Current",
                            compress = 'gzip',
                            models.chosen = "all",
                            build.clamping.mask = F,
                            output.format = '.RData',
                            do.stack=T
                            )

### Use the previous projection to create the ensemble projection

Solea_ensemble_proj_current <- BIOMOD_EnsembleForecasting(
                              bm.em = ensemble_model,
                              bm.proj = Solea_model_proj_current,
                              selected.models = 'all',
                              compress = 'gzip',
                              build.clamping.mask = F,
                              output.format = '.RData',
                              do.stack=T
                              )

### Add the prediction to a common data frame to compute EOF

## Extract prediction 
ensemble_proj_current <- get_predictions(Solea_ensemble_proj_current)
# Rename 
colnames(ensemble_proj_current) <- YM_vector[1]

if (YM_vector[date]== "1993.02"){
  prediction_df <- ensemble_proj_current
}else{
  prediction_df <- cbind(prediction_df, ensemble_proj_current)
}


### Rasterize each prediction and create a stack of each prediction layers 

NewProjPresEnsemble <- current_env[[1]]
values(NewProjPresEnsemble) <- (ensemble_proj_current[,1]/1000) # set predictions between 0 and 1

# Rename the raster 
NewProjPresEnsemble@file@name <- YM_vector[1]
NewProjPresEnsemble@data@names <- "habitat suitability Index"

prediction_stack <- stack(prediction_stack, NewProjPresEnsemble)

### Compute the MESS to determine the uncertainty of each prediction and and them in a raster stack

mess.raster <- mess(
  x = current_env,
  v = extract(current_env, Occurences[,2:3])
)

# Rename the raster
mess.raster@file@name <- YM_vector[1]

mess <- stack(mess, mess.raster)


############ Supplementary 

### Graph 
# 
# Projection <- as.data.frame(NewProjPresEnsemble, xy=T)
# 
# ggplot()+
#   geom_raster(data=Projection, mapping = aes(x=x, y=y, fill = habitat.suitability.Index))+
#   scale_fill_viridis_c()+
#   geom_sf(data = coastline_reg, 
#           size = 3, color = "black", fill = "lightgrey")+
#   coord_sf()+
#   theme_bw()+
#   labs(x = "Longitude",
#        y = "Latitude",
#        fill = "habitat_suitability_Index")+
#   ggtitle(paste0("Presence of ",species[sp]," in february 1993")) +
#   theme(plot.title = element_text(hjust = 0.5))
#   
# ## In bay of biscay
# 
# ggplot()+
#   geom_raster(data=Projection, mapping = aes(x=x, y=y, fill = habitat.suitability.Index))+
#   scale_fill_viridis_c()+
#   geom_sf(data = coastline_reg, 
#           size = 3, color = "black", fill = "lightgrey")+
#   coord_sf()+
#   theme_bw()+
#   xlim(-5,0)+
#   ylim(44,48.5)+
#   labs(x = "Longitude",
#        y = "Latitude",
#        fill = "Prediction")+
#   ggtitle(paste0("Presence of ",species[sp]," in february 1993")) +
#   theme(plot.title = element_text(hjust = 0.5))
#   

