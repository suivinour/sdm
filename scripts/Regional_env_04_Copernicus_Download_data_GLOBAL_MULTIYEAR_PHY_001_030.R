# ---------------------------------------------------------------------------- #
#           SUIVI NOUR    Mise a jour de la distribution des nourricerie
# ---------------------------------------------------------------------------- #
#  Load large scale Copernicus marine data.
# ---------------------------------------------------------------------------- #


# Load : https://data.marine.copernicus.eu/product/GLOBAL_MULTIYEAR_PHY_001_030/description
#-Sea water potential temperature at sea floor (bottomT)
#-Sea water salinity (S) : so
#-Sea water potential temperature (T), thetao


source(paste0("./scripts/boot.R"))


name.var <- c("so",
              "thetao") # For bottom extract only one layer is sufficient


# date_start <- seq(as.Date("1994-01-01"), as.Date("2020-11-30"), by = 'month') 
# 
# date_end <- as.Date("1994-01-31") %m+% months(0:length(date_start))[-(length(date_start) + 1)]


date_start <-
  seq(as.Date("1993-02-01"), as.Date("1993-01-31"), by = 'month')
date_end <-
  as.Date("1993-01-31") %m+% months(0:length(date_start))[-(length(date_start) +
                                                              1)]


month_year <- format(date_start, "%Y_%m")


lapply(name.var, function(var_i) {
  print(paste0(var_i, " --- initiated"))
  
  n.cores <- detectCores()
  clust <- makeCluster(n.cores - 1)
  registerDoParallel(clust)
  foreach::getDoParWorkers()
  
  
  
  clusterEvalQ(clust, {
    library("raster")
    library("akima")
    library("ggplot2")
    library("terra")
    library("stringr")
    library("RColorBrewer")
    library("tidyverse")
    library('RCMEMS')
  })
  
  foreach(month_i = 1:length(date_start),
          .verbose = TRUE) %dopar% {
           
             #lapply(1:length(date_start), function(month_i){
            
            print(paste0(date_start[month_i], "-------- initiated"))
            
            
            chemin_motuclient <-   "C:/Users/sfabriru/Anaconda3/Lib/site-packages/motuclient/motuclient.py"
            user.id  <- "xxx" # login user name for Copernicus
            password.id <- "xxx" # password for Copernicus
            service <- "GLOBAL_MULTIYEAR_PHY_001_030-TDS"
            product <- "cmems_mod_glo_phy_my_0.083_P1M-m"
            motu_site <-"https://my.cmems-du.eu/motu-web/Motu"# depend on the product, it can be : https://my.cmems-du.eu/motu-web/Motu
            variable <- var_i
            date_min <- date_start[month_i]
            date_max <- date_end[month_i]
            auth_mode = "cas"
            zone.mtq <- c(
              ymin = min_y_reg,
              ymax = max_y_reg,
              xmin = min_x_reg,
              xmax = max_x_reg
            )  %>% as.character()
            
            profs <- c(0.49402499198913574, 1245.291015625) %>% as.character()
            # script ----
            # Create forlders where the data will be downloadded
            folder_out <- paste0(path_to_data_raw,
                                 "Env_data/Copernicus/",
                                 "Copernicus_",
                                 service,
                                 "_",
                                 variable)
            dir.create(folder_out ,
                       recursive = T,
                       showWarnings = F)
            
            out_name <- paste0("Copernicus_", service,"_",variable,"_",month_year[month_i],".nc")
            
            configuration <-
              CMEMS.config(
                python = 'C:/Users/sfabriru/Anaconda3/python.exe',
                script = chemin_motuclient,
                user  = user.id,
                pwd = password.id,
                motu = motu_site,
                service.id = service,
                product.id = product,
                variable = variable,
                date.min = as.character(date_min),
                date.max = as.character(date_max),
                latitude.min = zone.mtq[1],
                latitude.max = zone.mtq[2],
                longitude.min = zone.mtq[3],
                longitude.max = zone.mtq[4],
                depth.min = profs[1],
                depth.max = profs[2],
                out.dir = folder_out,
                out.name = out_name
              )
            
            CMEMS.download(configuration, quiet = TRUE)
            
            print(paste0(date_min, "-------- completed"))
            
          }
  stopCluster(clust)
  
  
  
  #)
  print(paste0(var_i, " --- completed"))
  
})
