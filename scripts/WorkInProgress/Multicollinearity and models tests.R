########################## SUIVINOUR #######################################
############################################################################

### This script will add last environmental variables and
#check the multicollinearity in the Regional scale model dataset


### Calling all of the common object to my analyses
source("./scripts/boot.R")

# Start with Solea
sp = 1

### Import Occurences
Occurences <-  read_delim(paste0(path_to_data_tidy,"Occ/Regional/", species[sp],"_Regional_scale_occurences_env.csv"),delim = ",", col_select = c(2:18))


### Select the Occurences prior to 1993.09 as the env data are not complete

Occurences <- Occurences %>% 
  filter(YM >= 1993.09) 

### ---------------------- Resample Merlangius occurences ----------------------
#-------------------------------------------------------------------------------
#There is 2 times more presence than absence in the M.merlangus dataset. This could lead to 
# potential biais. It's recommended to have a 1:1 ratio between presence and absence 

if (species[sp] == "Merlangius merlangus"){

Occurences_abs <- Occurences %>% 
  filter(Occurence == 0)
## 11650 absence 

Occurences_pre <- Occurences %>% 
  filter(Occurence == 1) %>%  
## 32879 presence so i will resample 12000
  slice_sample(n = 12000)

Occurences <- rbind(Occurences_pre,Occurences_abs)

# ## Graph to check data
# ggplot(data = Occurences, aes(x = factor(Year), fill = Origin))+
#   geom_bar()+
#   labs(title= paste0("Yearly distribution of ",species[sp]," occurences"))+
#   labs(x = "Year",
#        y = "Number of observations",
#        fill = "Origin")+
#   scale_fill_manual(values=c('#8c510a','#d8b365','#f6e8c3','#c7eae5','#5ab4ac','#01665e'))+
#   theme_bw()+
#   theme(axis.text.x = element_text(angle = 90),
#         plot.title = element_text(hjust = 0.5))+
#   facet_grid(.~ Occurence)
# 
# # Spatial distrib
# ggplot()+
#   geom_sf(data = coastline_reg, 
#           size = 3, color = "black", fill = "grey")+
#   ggtitle(paste0("Spatial distribution of ",species[sp]," presence")) +
#   coord_sf()+
#   theme_bw()+
#   geom_point(Occurences_pre, 
#              mapping = aes(x = Long, y = Lat, colour = Origin),
#              shape = 19, alpha = 0.5)+
#   scale_colour_manual(values=c('#8c510a','#d8b365','#f6e8c3','#c7eae5','#5ab4ac','#01665e'))+
#   labs(x = "Longitude",
#        y = "Latitude",
#        colour = "Origin")+
#   theme(plot.title = element_text(hjust = 0.5))
}

###----------------------- Compute DO% Sat -------------------------------------
# library(rMR)
# 
# # transform mole concentration in concentration
# 
# Occurences$o2_bottom <- (Occurences$o2_bottom / 100) * 16
# 
# Occurences$DO_sat <- DO.saturation(Occurences$o2_bottom, temp.C = Occurences$bottomT,
#                                    elevation.m = 0)



## Keep environment to check the multicolinearity
Occurence_env <- Occurences %>%
  dplyr::select(o2_bottom,so_bottom,bottomT,NAO, chl, nppv,bathymetry)

#### ---------------------------------------------------------------------------

### There is two ways to check the potential colinearity among variables, 
### Pearson's correlation matrix and VIF
## Threshold for Pearson correlation is 0.7
## Threshold for VIF is around 10

## Pearson
library(correlation)

correlation(Occurence_env, include_factors = T, method = "pearson")
## Chla and nppv are the only correlated variable

# Plot the correlation
library(ecospat)

pdf(paste0(path_to_figures,"Correlation of ",species[sp],".pdf"), width = 10, height = 5)
ecospat.cor.plot(Occurence_env)

dev.off()


## Remove nppv 
Occurence_env <- Occurence_env %>% 
  dplyr::select(-nppv)

correlation(Occurence_env, include_factors = T, method = "pearson") # No correlation

## Test with VIF to make sure that there is no hidden correlation

library(usdm)

VIF <- vif(as.data.frame(Occurence_env[,1:6]))
# Every variable is below 5 so it's good

ggplot(data = VIF, mapping = aes(x = Variables, y = VIF))+
  geom_histogram(stat = "identity")+
  theme_bw()+
  ggtitle(paste0(species[sp], " VIF test")) +
  theme(plot.title = element_text(hjust = 0.5))
ggsave(paste0(species[sp], " VIF test",".pdf"),width = 7, height = 5, units = "in",path = path_to_figures)


# -----------------------------------------------------------------------------

# Essai modelisation
library(biomod2)
#============================================================================================================
# The first step is to format the data for Biomod2 

myRespName = "Solea_solea_Regional"

bmData <- BIOMOD_FormatingData(resp.var= Occurences$Occurence,
                               
                               expl.var= Occurences[,10:16], # a matrix, data.frame, SpatialPointsDataFrame or RasterStack 
          #containing your explanatory variables that will be used to build your models
          
                               resp.xy = Occurences[,2:3], # X and Y coordinates of resp.var
                               resp.name = myRespName)
summary(bmData)
# ===========================================================================================================
# 2. Defining Models Options using default options.
myBiomodOption <- BIOMOD_ModelingOptions()

# ===========================================================================================================
# 3. Computing the models

mySDMModel <- BIOMOD_Modeling(bm.format=bmData,
                              
                              models = c('RF'), # c('GLM', 'GBM', 'GAM', 'CTA', 'ANN', 'SRE', 'FDA', 'MARS', 'RF') 
                              
                              bm.options = myBiomodOption,
                              
                              nb.rep = 4, # Number of Evaluation run
                              
                              do.full.models = FALSE, # if true, models calibrated and evaluated with the whole dataset are done
                              
                              data.split.perc = 80, # % of data used to calibrate the models, the remaining part will be used for testing
                              nb.cpu=5,
                              var.import = 3,# Number of permutation to estimate variable importance
                              
                              metric.eval = c('KAPPA','TSS','ROC'), # names of evaluation metrics
                              
                              save.output = TRUE, # keep all results and outputs on hard drive or not 
                              modeling.id = paste(myRespName,"GBM_Modeling",sep="")
                              )

get_evaluations( mySDMModel)
  get_scaling_model
getters.bm(Solea.solea.Regional_allData_RUN1_RF)


scaling.models<-get_scaling_model(Solea.solea.Regional_allData_RUN1_RF)
resid<-residuals(scaling.models)
load("C:/Users/sfabriru/Documents/Research/SuiviNour/R/SuiviNour/SDM/Solea.solea.Regional/Solea.solea.Regional.Solea_solea_RegionalGBM_Modeling.models.out")

load("C:/Users/sfabriru/Documents/Research/SuiviNour/R/SuiviNour/SDM/Solea.solea.Regional/models/Solea_solea_RegionalGBM_Modeling/Solea.solea.Regional_allData_RUN1_RF")

load("C:/Users/sfabriru/Documents/Research/SuiviNour/R/SuiviNour/SDM/Solea.solea.Regional/.BIOMOD_DATA/Solea_solea_RegionalGBM_Modeling/calib.lines")
load("./Solea.solea.Regional/.BIOMOD_DATA/Solea_solea_RegionalGBM_Modeling/formated.input.data")

Solea.solea.Regional_allData_RUN1_RF@model
resid(Solea.solea.Regional_allData_RUN1_RF@model)
pred<-get_predictions(Solea.solea.Regional.Solea_solea_RegionalGBM_Modeling.models.out)


calib<-get_calib_lines(Solea.solea.Regional.Solea_solea_RegionalGBM_Modeling.models.out)
pred$pred[pred$run=="RUN1"]

test<-BIOMOD_Projection(Solea.solea.Regional.Solea_solea_RegionalGBM_Modeling.models.out,
                  Occurences[,10:16], proj.name="predict_sp")

y_hat<-test@proj.out@val$pred[test@proj.out@val$run=="RUN1"]/1000


Z=(Y−p^)/p^(1−p^)
. 
# Given an inevitable nonparametric model structure, a residual in the traditional sense is not possible.
# You could approximate the response, Pearson's or deviance (likelihood ratio chi-squared) residual error using 
# standard methods found in the logistic regression literature, eg., response = [y - y-hat],
# Pearson's = [(y - y-hat) / sqrt(y-hat)] where; y = observed, y-hat = estimated probability. 
# However, you do not know the curvilinear nature of the model so, cannot account for it in evaluating the residual error.

y<-Occurences$Occurence

pearson_resid<-(y - y_hat) / sqrt(y_hat*(1-y_hat))
# ===============================================================================================================
# 4. Model evaluation

SDMscores <- get_evaluations(mySDMModel)

SDMscores <- SDMscores %>% 
  group_by(metric.eval) %>% 
  summarise(mean(validation))


## Relative importance of the explanatory variables

get_variables_importance(mySDMModel)
(models_var_import <-get_variables_importance(mySDMModel))

var_importance <- models_var_import %>% 
  group_by(expl.var) %>% 
  summarise(mean(var.imp))

# Graphic representation

pdf(paste0(path_to_figures,"Regional scale occurences/Influence of algorithm for ",species[sp]," models.pdf"), width = 7, height = 5)
bm_PlotEvalMean(bm.out=mySDMModel, group.by = "run",metric.eval = c("ROC","TSS"), xlim = c(0,1), ylim = c(0,1))
dev.off()

### We can now represent the response curve of:
# 1. All the response curves from all the algorithms at once
# 2. All the response curves per algorithm
# The response curves show how each algorithm describes each environmental response 
# in relation with the species. This will allow to check if the niche was correctly modeled.

pdf(paste0(path_to_figures,"Regional scale occurences/Influence of algorithm for ",species[sp]," models.pdf"), width = 7, height = 5)
bm_PlotResponseCurves(bm.out = mySDMModel,
               
                      new.env = get_formal_data(mySDMModel,'expl.var'),
               
                      show.variables = get_formal_data(mySDMModel,'expl.var.names'),
              
                      do.bivariate = FALSE,
              
                      fixed.var = 'median',
               
                      col = brewer.pal(09, "Spectral"),
               
                      legend = TRUE,
               
                      data_species = get_formal_data(mySDMModel,'resp.var'))
dev.off()




