# ---------------------------------------------------------------------------- #
#           SUIVI NOUR    Mise a jour de la distribution des nourricerie
# ---------------------------------------------------------------------------- #
# This script is to prepare files for interolation
# ---------------------------------------------------------------------------- #


source(paste0("./scripts/boot.R"))

################################################################################################
################################ Prepare the grid file for interpolation
################################################################################################

# Load layer with the target resolution
# from https://data.marine.copernicus.eu/product/IBI_ANALYSISFORECAST_PHY_005_001/description

if (file.exists(paste0(path_to_data_tidy, "new_grid_loc.tif")) ==
    FALSE) {
  new_grid <- raster(
    paste0(
      path_to_data_raw,
      "cmems_mod_ibi_phy_anfc_0.027deg-2D_PT15M-i_1674546423491.nc"
    )
  )
  
  new_grid <- extend(x = new_grid,
                     y = c(min_x_loc,
                           max_x_loc,
                           min_y_loc,
                           max_y_loc))# expend the grid to the target domain
  
  new_grid <- crop(x = new_grid,
                   y = c(min_x_loc,
                         max_x_loc,
                         min_y_loc,
                         max_y_loc))
  
  names(new_grid) <- 'values'
  values(new_grid) <- 1
  
  new_grid.df <- as_tibble(rasterToPoints(new_grid))#transform in df
  
  ################# Plot of the target resolution data #################
  
  p1<-ggplot(new_grid.df) +
            geom_raster(aes(x=x, y=y, fill = values), show.legend = FALSE) +
            coord_equal()+
            #geom_sf(data=coastline_reg, fill="grey70")+
            ylab('')+
            xlab('')+
            ggtitle('Target resolution')
  
  
  ################ Rasterize coastline to generate a mask on the target grid #################
  
  coast_rast <- fasterize(coastline_reg,
                          raster = new_grid)
  
  coast_rast[is.na(coast_rast$layer)] <- 999
  coast_rast[coast_rast$layer == 1] <- NA
  #plot(coast_rast)
  names(coast_rast) <- 'values'
  
  ################# Apply the mask to new grid #################
  
  new_grid <- coast_rast
  names(new_grid) <- 'values'
  new_grid <- crop(new_grid, extent(min_x_loc,
                                    max_x_loc,
                                    min_y_loc,
                                    max_y_loc))#crop to the regional scale domain
  
  
   new_grid.df<-as_tibble(rasterToPoints(new_grid))#transform in df
  
  #Plot of the target resolution data
  p3<-ggplot(new_grid.df) +
    geom_raster(aes(x=x, y=y, fill = values),  show.legend = FALSE) +
    ylab('')+
    xlab('')+
    ggtitle('New grid without continent')
  
  coast.df<-as_tibble(rasterToPoints(coast_rast))#transform in df
  
  p2<-ggplot(coast.df) +
    geom_raster(aes(x=x, y=y, fill = values)) +
    ylab('')+
    xlab('')+
    ggtitle('Rasterize coastline to generate a mask on the target gridd')
  
  ################# Load WKT files which is a file to remove Med Sea #################
  
  WKT <-st_read(str_c(paste0(path_to_data_raw, 'coastline_outline/WKT-point.shp')), quiet = TRUE) |>
    summarise(geometry = st_combine(geometry)) %>%
    st_cast("POLYGON")
  
  new_grid_no_Med <- fasterize(WKT,
                               raster = new_grid) #rasterize with new_grid resolution
  
  new_grid_final <- mask(new_grid_no_Med,
                         new_grid)# apply the mask to remove continent and get the final new_grid
  
  names(new_grid_final) <- 'values'
  
  new_grid_final.df <-  as_tibble(rasterToPoints(new_grid_final))#transform in df
  
  new_grid_final.df <- as.data.frame(rasterToPoints(new_grid_final))#transform in df
  
  writeRaster(new_grid_final,
              paste0(path_to_data_tidy, "new_grid.tif"),
              overwrite = T)
  
}

new_grid_final <- raster(paste0(path_to_data_tidy,
                                "new_grid.tif"))

if (file.exists(paste0(path_to_data_tidy, "bathy_regrid.tif")) ==
    FALSE) {

  bathy_regrid <- resample(bathymetry, new_grid_final)
  
  writeRaster(bathy_regrid,
              paste0(path_to_data_tidy, "bathy_regrid.tif"),
              overwrite = T)
  
  bathy_regrid_200 <- resample(bathymetry,
                               new_grid_final)
  
  bathy_regrid_200[bathy_regrid_200 < (-200)] <- NA
  
  writeRaster(bathy_regrid_200,
              paste0(path_to_data_tidy, "bathy_regrid_200.tif"),
              overwrite = T)
  
  
  
}


bathy_regrid <- raster(paste0(path_to_data_tidy,
                              "bathy_regrid.tif"))

bathy_regrid_200 <- raster(paste0(path_to_data_tidy,
                                  "bathy_regrid_200.tif"))
