# ---------------------------------------------------------------------------- #
#           SUIVI NOUR    Mise a jour de la distribution des nourricerie                 
# ---------------------------------------------------------------------------- #
# 07. Delta model for local scale
# ---------------------------------------------------------------------------- #

## This script was the previous version of Local_occ_07_Categorical_Delta_model.R
###
## In this script, i use continuous variables to perform Delta model
###
## There is also a separation of the dataset in calibration and validation that can 
## be used if you want to asses the quality of the model. 


### Calling all of the common object to my analyses
source("./scripts/boot.R")

sp = 1

### Import Occurences
Occurences <-  read_delim(paste0(path_to_data_tidy,"Occ/Local/", species[sp],"_Local_scale_model_data.csv"),delim = ",")

### Set covariate as factor
Occurences$substrat <- as.factor(Occurences$substrat)
Occurences$wave_mean <- as.factor(Occurences$wave_mean)
Occurences$bathymetry <- as.factor(Occurences$bathymetry)
Occurences$sali_mean <- as.factor(Occurences$sali_mean)
Occurences$temp_cv <- as.factor(Occurences$temp_cv)
Occurences$temp_perc_90 <- as.factor(Occurences$temp_perc_90)
Occurences$chl_mean <- as.factor(Occurences$chl_mean)

#--------------- Generating cross validation  ----------------------------------

## We create a separation between the training set and the testing set of data
## to ensure that the data are fully independant 

## Set seed for reproductibility

set.seed(104)

Partition <- sample.int(n = nrow(Occurences), size = floor(.75*nrow(Occurences)), replace = F)

## Separate validation and calibration dataset

Occurences_calib <- Occurences[Partition,]
Occurences_valid <- Occurences[-Partition,]

#Select presence data (Occurence = 1)
Occurences_calib_gaussian <- Occurences_calib %>%
  filter(Occurence == 1) %>%
  # Transform the density in log(density)
  mutate(Log_Density = log(Density+1))

#Select presence data (Occurence = 1)
Occurences_valid_gaussian <- Occurences_valid %>%
  filter(Occurence == 1) %>%
  # Transform the density in log(density)
  mutate(Log_Density = log(Density+1))


############################# Modelisation #####################################
################################################################################

#--------------------------- Model delta ---------------------------------------
  
  ###------------------------ Binomial model--------------------------------------
  #-------------------------------------------------------------------------------
  
  ### Selection of the best model based on AIC criterion

  Binomial_glm_start <- glm(formula = Occurence ~ 1,
                            family = binomial(link = "logit"),
                            data = Occurences_calib)
  
  glm_formula <- formula('Occurence ~ 1 + chl_mean + substrat + bathymetry + temp_cv + wave_mean + temp_perc_90 + sali_mean')
  
  Binomial_glm <- stepAIC(object = Binomial_glm_start,
                          scope = glm_formula,
                          data = Occurences_calib,
                          direction = 'both',
                          trace = FALSE,
                          k = 2)
  

  ### Keep only variables with significant effect
  if(species[sp]== "Solea solea"){
    Binomial_glm <- glm(Occurence ~ bathymetry + substrat + temp_perc_90,
                        family = binomial(link = "logit"),
                        data = Occurences_calib)
  }
  
  
  
  
### ------------------------- Evaluation of model ------------------------------
### 

### ANOVA of the best model by AIC---------------------------------------------------
summary(Binomial_glm)
ANOVA <- anova(Binomial_glm,test="Chisq")
## get AIC
Binomial_glm$anova$AIC


### Percent of explained deviance ---------------------------------------------
pc_dev_expl <- anova(Binomial_glm)[-1,2]*100 / anova(Binomial_glm)[1,4]
# % explained per factor
print( pc_dev_expl , digit = 2)

sum(pc_dev_expl)
# % explained per df
print( pc_dev_expl / anova(Binomial_glm)[-1,1], digit = 2)


### Plot the ROC curve and the AUC (Area under curve) for the calibration data
png(filename = paste0(path_to_figures,"Local scale occurences/",species[sp],"Calibration_ROC_curve.png"),
    width = 500,height = 400,units = "px")

roc.plot(Occurences_calib$Occurence, fitted(Binomial_glm),main="ROC Curve - Calibration data")
legend(x="topleft",legend=paste("Area=",round(roc.area(Occurences_calib$Occurence, fitted(Binomial_glm))$A,digits=3),sep=''),text.col='red',cex=1.6)
dev.off()

### Retrieve AUC value
AUC_calib <- roc.area(Occurences_calib$Occurence, fitted(Binomial_glm))

### Retrieve fitted values
Occurences_calib$fitted <- Binomial_glm$fitted


### Plot the ROC curve and the AUC (Area under curve) for the validation data
#Fit the model with new data
Occurences_valid$fitted <- predict.glm(Binomial_glm,newdata=Occurences_valid,type="response")

png(filename = paste0(path_to_figures,"Local scale occurences/",species[sp],"Validation_ROC_curve.png"),
    width = 500,height = 400,units = "px")
roc.plot(Occurences_valid$Occurence, Occurences_valid$fitted,main="ROC Curve - Validation data")
legend(x="topleft",legend=paste("Area=",round(roc.area(Occurences_valid$Occurence, Occurences_valid$fitted)$A,digits=3),sep=''),text.col='red',cex=1.6)
dev.off()

### Retrieve AUC value
AUC_valid <- roc.area(Occurences_valid$Occurence, Occurences_valid$fitted)


    ###------------------------ Gaussian model--------------------------------------
    #-------------------------------------------------------------------------------


### Selection of the best model by AIC criteron

Gaussian_glm_start <- glm(formula = Log_Density ~ 1,
                          family = gaussian,
                          data = Occurences_calib_gaussian)

glm_formula <- formula('Log_Density ~ 1 + chl_mean + substrat + bathymetry + temp_cv + wave_mean + temp_perc_90 + sali_mean')

Gaussian_glm <- stepAIC(object = Gaussian_glm_start,
                        scope = glm_formula,
                        data = Occurences_calib_gaussian,
                        direction = 'both',
                        trace = FALSE,
                        k = 2)

if(species[sp]=="Solea solea"){

  Gaussian_glm <- glm(Log_Density ~ bathymetry + sali_mean + temp_cv + temp_perc_90 + substrat + o2_perc_25 + wave_mean,
                      family = gaussian,
                      data = Occurences_calib_gaussian)
}else{
  
Gaussian_glm <- glm(Log_Density ~ o2_perc_25 + substrat + temp_perc_90 + bathymetry + sali_mean ,
                    family = gaussian,
                    data = Occurences_calib_gaussian)
}

###-------------------------- Evaluation of model ------------------------------

### Anova and AIC for the model

summary(Gaussian_glm)
ANOVA_glm <- anova(Gaussian_glm,test="Chisq")

ANOVA_glm$AIC <- Gaussian_glm$anova$AIC

### We compute the percent of deviance explained by each variable to the model
pc_dev_expl <- anova(Gaussian_glm)[-1,2]*100 / anova(Gaussian_glm)[1,4]

# % explained per factor
print( pc_dev_expl , digit = 2)

sum(pc_dev_expl)
# % explained per degree of freedom
print( pc_dev_expl / anova(Gaussian_glm)[-1,1], digit = 2)

### Evaluation metrics calibration ---------------------------------------------

### We retrieve the fitted values of the model
Occurences_calib_gaussian$fitted <- Gaussian_glm$fitted

### Computation of the RMSE (Root Mean Square Error)
RMSE <- sqrt(mean((Occurences_calib_gaussian$Log_Density - Occurences_calib_gaussian$fitted)^2))

### Compute the R² between observed and fitted values 
reg_all <- lm(Log_Density ~ fitted, 
              data = Occurences_calib_gaussian)

R_squared <- summary(reg_all)$r.squared

### Plot the regression
png(filename = paste0(path_to_figures,"Local scale occurences/",species[sp],"Calibration_gaussian_fitted.png"),
    width = 500,height = 400,units = "px")
plot(Occurences_calib_gaussian$fitted, Occurences_calib_gaussian$Log_Density,
     main="Calibration data - Log(positive density)",xlab="Predictions",ylab="Observations",ylim=c(0,8))

lines(x=c(0,max(Occurences_calib_gaussian$fitted)),y=c(reg_all$coefficients[1],reg_all$coefficients[1]+reg_all$coefficients[2]*max(Occurences_calib_gaussian$fitted)),col='red',lwd=2)
legend(x="bottomright",legend=c(paste("y=",round(reg_all$coefficients[2],digits=3),"*x+",round(reg_all$coefficients[1],digits=3),sep=''),paste("R²=",round(summary(reg_all)$r.squared,digits=3),sep='')),text.col='red',cex=1.4)
dev.off()

# Test if the residuals are trended
### Relation between the residuals and the fitted values 
reg_all <- lm(fitted(Gaussian_glm) ~ rstandard(Gaussian_glm))

summary(reg_all)
summary(reg_all)$r.squared

plot(fitted(Gaussian_glm),rstandard(Gaussian_glm),xlab="Fitted model values",ylab="Standardized variance residuals")


### Compute abundance assesment metrics (Waldock)

abundance_metrics <- abundance_assessment_metrics(predictions = Occurences_calib_gaussian$fitted, observations = Occurences_calib_gaussian$Log_Density)

### Retrieve usefull metrics
Calibration_metrics <- data.frame(AUC_calib$A,RMSE, R_squared, (1- abundance_metrics$Amae_rel_mean), abundance_metrics$Dpearson, abundance_metrics$Pdispersion)
colnames(Calibration_metrics) <- c("AUC","RMSE","R²","1-Amae","Dpearson","Pdisp")


### Evaluation metrics validation --------------------------------------------------

Occurences_valid_gaussian$fitted <- predict.glm(Gaussian_glm,newdata=Occurences_valid_gaussian,type="response")

### Compute RMSE
RMSE_valid <- sqrt(mean((Occurences_valid_gaussian$Log_Density - Occurences_valid_gaussian$fitted)^2))


### Relation between the observed and the predicted values 
reg_all <- lm(Log_Density ~ fitted, 
              data = Occurences_valid_gaussian)

R_squared_valid <- summary(reg_all)$r.squared

### Plot the regression
png(filename = paste0(path_to_figures,"Local scale occurences/",species[sp],"Validation_gaussian_fitted.png"),
    width = 500,height = 400,units = "px")
plot(Occurences_valid_gaussian$fitted, Occurences_valid_gaussian$Log_Density,
     main="Validation - Log(positive density)",xlab="Predictions",ylab="Observations")
lines(x=c(min(Occurences_valid_gaussian$fitted),max(Occurences_valid_gaussian$fitted)),y=c(reg_all$coefficients[1],reg_all$coefficients[1]+reg_all$coefficients[2]*max(Occurences_valid_gaussian$fitted)),col='red',lwd=2)
legend(x="bottomright",legend=c(paste("y=",round(reg_all$coefficients[2],digits=3),"*x+",round(reg_all$coefficients[1],digits=3),sep=''),paste("R²=",round(summary(reg_all)$r.squared,digits=3),sep='')),text.col='red',cex=1.4)
dev.off()

### Compute abundance assesment metrics (Waldock)

abundance_metrics_valid <- abundance_assessment_metrics(predictions = Occurences_valid_gaussian$fitted, observations = Occurences_valid_gaussian$Log_Density)

### Retrieve usefull metrics
Validation_metrics <- data.frame(AUC_valid$A,RMSE_valid, R_squared_valid, (1-abundance_metrics_valid$Amae_rel_mean), abundance_metrics_valid$Dpearson, abundance_metrics_valid$Pdispersion)
colnames(Validation_metrics) <- c("AUC","RMSE","R²","1-Amae","Dpearson","Pdisp")


##### Represent metrics

 Metrics <- rbind(Calibration_metrics,Validation_metrics)
 rownames(Metrics) <- c("Calibration","Validation")
# 
 
################################ Plotting metrics ##############################
################################################################################
 ## Lollipop--------------------------------------------------------------------
 
### Reorder df and delet RMSE
Metrics_plot <- data.frame(t(Metrics))
Metrics_plot$x <- factor(rownames(Metrics_plot))
Metrics_plot <- subset(Metrics_plot, Metrics_plot$x != "RMSE")

ggplot(data = Metrics_plot)+
  geom_segment(aes(x=factor(x,levels = c("Pdisp","Dpearson","1-Amae","R²","AUC")), xend=factor(x,levels = c("Pdisp","Dpearson","1-Amae","R²","AUC")), y=Calibration, yend=Validation), color="black")+
  geom_point( aes(x=factor(x,levels = c("Pdisp","Dpearson","1-Amae","R²","AUC")), y=Calibration, color= "Calibration"), size=2 ) +
  geom_point( aes(x=factor(x,levels = c("Pdisp","Dpearson","1-Amae","R²","AUC")), y=Validation, color= "Validation"), size=2 ) +
  scale_color_manual(values = c("lightblue", "indianred"),
                     guide  = guide_legend(), 
                     name   = "")+
  geom_hline(yintercept=1, linetype = 5)+
  scale_y_continuous(breaks = seq(0, 1, by = 0.2))+
  coord_flip()+
  theme_bw()+
  xlab("")+
  ylab("")+
  theme(axis.text = element_text(size = 12))
ggsave(plot = last_plot(), paste0("Metrics evaluation of ",species[sp]," Delta model.png"),
       path = paste0(path_to_figures,"Local scale occurences/"),width = 5,height = 2, units = "in")

  
###-------------- Combining binomial and gaussian model prediction -------------
#-------------------------------------------------------------------------------

## Predict the presence probability on the raw data set
Occurences$pres.prob <- predict.glm(Binomial_glm,newdata=Occurences,type="response")

## Predict the log density on the raw data set
Occurences$log.dens <- predict.glm(Gaussian_glm,newdata=Occurences,type="response")

## Compute the correction to combine the two prediction (est.dens)
sigma2 <- var(resid(Gaussian_glm))

Occurences$est.dens <- Occurences$pres.prob*exp(Occurences$log.dens)*exp(sigma2/2)



### --------------- Plot the response curve of each variable -------------------
#-------------------------------------------------------------------------------
### Retrieve the names of all variables used in binomial and gaussian model
Variables <- unique(c(all.vars(formula(Binomial_glm)[-2]),all.vars(formula(Gaussian_glm)[-2])))
### Keep continuous variables
Variables <- Variables[!Variables %in% c("substrat","wave_mean")]

### Get dataset with correct variables and names
Response_curve_df <- Occurences %>% 
  ## Select only continous variables (no substrate and wave mean)
  dplyr::select(est.dens,all_of(Variables)) %>%  
  ## Create a collumn for the names of variable and one for the value
  pivot_longer(cols = Variables,names_to = "Variable",values_to = "Value") 

Response_curve_df$Value <- round(Response_curve_df$Value, digits = 1)
  ## Compute the mean and quantile
Response_curve_df <- Response_curve_df %>% 
  group_by(Variable,Value) %>% 
  mutate(response_mean = mean(est.dens),response_sd = sd(est.dens))

if (species[sp]== "Solea solea"){
  ## Rename variables
Response_curve_df$Variable <- recode(Response_curve_df$Variable, 
                                     bathymetry = "Bathymetrie (m)",
                                     temp_perc_90 = "Temperature (°C)",
                                     temp_cv = "Variation de temperature (°C)",
                                     o2_perc_25 = "Saturation en oxygene (%)",
                                     sali_mean = "Salinite")
}else{
  Response_curve_df$Variable <- recode(Response_curve_df$Variable, 
                                       bathymetry = "Bathymetrie (m)",
                                       temp_perc_90 = "Temperature (°C)",
                                       temp_cv = "Variation de temperature (°C)",
                                       o2_perc_25 = "Saturation en oxygene (%)",
                                       o2_cv = "Variation de saturation (%)",
                                       nppv_mean = "NPP (mgC/m3/j)",
                                       sali_mean = "Salinite")
}

## Plot Responses curves
ggplot(Response_curve_df, mapping = aes(x = Value, y = response_mean)) +
  facet_wrap(~Variable, scales = "free_x")+
  geom_point(alpha = 0.1,cex=0.2) +
  #geom_smooth(method = "gam", colour = "black")+
  geom_rug(sides = "b") +
  ylab("Densite (ind/ha)") +
  xlab("")+
  ylim(0,100)+
  theme_bw()+
  theme(axis.text.x = element_text(angle = 45, hjust = 1),
        axis.text = element_text(size = 12),strip.text = element_text(size = 12))
ggsave(plot = last_plot(), paste0("Variable responses curves of ",species[sp]," Delta models .png"),path = paste0(path_to_figures,"/Local scale occurences/"),
       width = 7.5,height = 5.58, units = "in")
 
## For Substrate  
## Import the correction for substrat
Correction <- read_delim(paste0(path_to_data_raw,
                                "Env_data/SHOM/SEDIM_Mondiale/Substrat_type_correction.csv"),col_select = c("Substrat","Valeur"))
Correction$substrat <- factor(Correction$Valeur)

Response_curve_substrat <- Occurences %>% 
  ## Select only factorial variables (no substrate and wave mean)
  dplyr::select(est.dens,substrat) %>% 
  left_join(Correction[,c("Substrat","substrat")])

Substrat <- ggplot(Response_curve_substrat, mapping = aes(x = Substrat, y = est.dens)) +
  geom_boxplot() +
  ylab("Density (ind/ha)") +
  xlab("")+
  ylim(0,75)+
  theme_bw()+
  theme(axis.text=element_text(size=15),
        axis.title=element_text(size=15))
ggsave(plot = Substrat, paste0("Substrat Responses curves of ",species[sp]," Delta model.png"),
       path = paste0(path_to_figures,"Local scale occurences/"),width = 5.2,height = 3.5, units = "in")


## For Wave mean  
Response_curve_wave <- Occurences %>% 
  ## Select only factorial variables (no substrate and wave mean)
  dplyr::select(est.dens,wave_mean) 

Wave <- ggplot(Response_curve_wave, mapping = aes(x = wave_mean, y = est.dens)) +
  geom_boxplot() +
  scale_x_discrete(labels =  c("<0.3","[0.3-0.5]",">0.5"))+
  ylab("Density (ind/ha)") +
  xlab("")+
  ylim(0,100)+
  theme_bw()+
  theme(axis.text=element_text(size=15),
        axis.title=element_text(size=15))
ggsave(plot = Wave, paste0("Wave Responses curves of ",species[sp]," Delta model.png"),
       path = paste0(path_to_figures,"Local scale occurences/"),width = 4.5,height = 3.5, units = "in")

  
###------------------ Mapping species density ----------------------------------
#-------------------------------------------------------------------------------

  ## The goal of this part is to predict the density of each species for the 1993 to 
  ## 2021 period. To do so, we predict the density for each year. 
  year = c(1993 : 2021)
  Predicted_values_df <- c()
  Raster_stack <- stack()
  MESS_stack <- stack()
  ## To map the species density, we download environment for each year
  
  path_to_environment <-paste0(path_to_data_tidy,"Env_data/Local_Projection_Environment/",list.files(paste0(path_to_data_tidy,"Env_data/Local_Projection_Environment/"),pattern = ".gri"))
  
  
  for(Date in (1 : length(path_to_environment))){
  
  Current_env <- stack(path_to_environment[Date]) 
  Current_env_model <- as.data.frame(stack(path_to_environment[Date]),xy=TRUE)
  
  Current_env_model$substrat <- as.factor(Current_env_model$substrat)
  Current_env_model$wave_mean <- as.factor(Current_env_model$wave_mean)
  
  ## Substrat class number 1 (cailloutis) is not present in the model
  if(length(Current_env_model$substrat[Current_env_model$substrat == 1])>0){
    Current_env_model$substrat[Current_env_model$substrat == 1] <- NA
  }
  
  ### Prediction of models ------------------------------------------------------
  
  ### Predict the binomial response
  Current_env_model$pres.prob <- predict.glm(Binomial_glm,newdata=Current_env_model[,all.vars(formula(Binomial_glm)[-2])],type="response")
  
  ### Predict the gaussian response
  Current_env_model$log.dens <- predict.glm(Gaussian_glm,newdata=Current_env_model[,all.vars(formula(Gaussian_glm)[-2])],type="response")
  
  ### Compute the corection
  sigma2 <- var(resid(Gaussian_glm))
  
  Current_env_model$est.dens <- Current_env_model$pres.prob*exp(Current_env_model$log.dens)*exp(sigma2/2)
  
  
  ### Retrieve the predicted values of density
  Values <- data.frame(density = na.omit(Current_env_model$est.dens), Year = year[Date])
  
  Predicted_values_df <- rbind(Predicted_values_df,Values)
  
  ### Map the predicted values
  Raster_map <- rasterFromXYZ(Current_env_model[,c("x","y","est.dens")])
  names(Raster_map) <- year[Date]
  
  ### Stack the rasters
  Raster_stack <- stack(Raster_stack,Raster_map)
  
  ### Compute MESS
  MESS <- dismo::mess(
    x = Current_env,
    v = extract(Current_env, Occurences[,c("Long","Lat")],na.rm = T))
  
  MESS_stack <- stack(MESS_stack,MESS)
  
  }
  
  ### Create folder outputs
  path_to_projection <- "//iota1/NourEnvData2023/SDM_Nourricerie/R/Projection/"
  
  folder_output_projection <- paste0(path_to_projection,
                                      "Local/Projection/")
  dir.create(folder_output_projection,
             recursive = T,
             showWarnings = F)
  
  ### Save predicted values
  write.csv(Predicted_values_df,file=paste0(folder_output_projection,species[sp]," predicted_density.csv"))
  
  ### Save projections stack
  writeRaster(Raster_stack, paste0(folder_output_projection,species[sp]," projected_density.grd"),overwrite = TRUE)

  ##############################################################################
  ##############################################################################
  
  ## Test the quantile regression between density and Year for different quantile
  
  library(quantreg)
  library(scales)
  
  quantiles <- c(0.5, 0.75, 0.90)
  
  fit_quantiles <- rq(density ~ Year,
                      tau = quantiles,
                      data = Predicted_values_df)
  summary(fit_quantiles)
  
  ### Plot
  png(filename = paste0(path_to_figures,"Local scale occurences/",species[sp],"Quantile regression.png"),
      width = 500,height = 400,units = "px")
  plot(density ~ Year, data = Predicted_values_df, cex = 0.1,ylab=c("Density (ind/ha)"),xlab="")
  abline(rq(density ~ Year, data = Predicted_values_df,tau = 0.5), col = "blue", lty = 2)
  abline(rq(density ~ Year, data = Predicted_values_df,tau = 0.75), col = "red", lty = 2)
  abline(rq(density ~ Year, data = Predicted_values_df,tau = 0.9), col = "green", lty = 2)
  legend("topright", legend = c("0.5", "0.75","0.9"), col = c("blue", "red","green"), lty = 2)
  dev.off()
  
  
  ### Map the juveniles density and standard error
  
  # Compute the standard deviation
  raster_sd <- calc(x=Raster_stack,
                          fun = function(x) sd(x,na.rm = T))
  
  ggplot()+
    geom_raster(data = as.data.frame(raster_sd,xy=TRUE), mapping = aes(x = x,y = y,fill = layer))+
    scale_fill_gradient(low = "lightblue", high = "indianred",na.value = "white")+
    geom_sf(data = coastline_loc,
            size = 3, color = "black", fill = "grey")+
    coord_sf()+
    theme_minimal()+
    labs(x = "",
         y = "",
         fill = "ecart-type")+
    theme(plot.title = element_text(hjust = 0.5),
          axis.text = element_text(size = 12))
  ggsave(plot = last_plot(), paste0("Standard deviation of ",species[sp]," predictions.png"),
         path = paste0(path_to_figures,"Local scale occurences/Projections"),width = 5.57,height = 5.58, units = "in")
  
  
  
###################### Distribution of juvenils ################################
################################################################################
  
  ## Compute the mean density for the 1993 - 2021 period
  Mean_density <- mean(Raster_stack)
  
  
  ## Set classes of density
  Q0 <- round(Mean_density@data@min,digits = 1)
  Q25 <- round(quantile(na.omit(Mean_density), probs = 0.25, names = F),digits = 1)
  Q50 <- round(quantile(na.omit(Mean_density), probs = 0.5, names = F),digits = 1)
  Q75 <- round(quantile(na.omit(Mean_density), probs = 0.75, names = F),digits = 1)
  Q90 <- round(quantile(na.omit(Mean_density), probs = 0.90, names = F),digits = 1)
  Q100 <- round(quantile(na.omit(Mean_density), probs = 1, names = F),digits = 1)
  

  ## Plot the density
  ggplot()+
    geom_raster(data = as.data.frame(Mean_density,xy=TRUE), 
                mapping = aes(x = x,
                              y = y,
                              fill = cut(layer, c(Q0,Q25,Q50,Q75,Q90,Q100))))+
    geom_tile() +
    scale_fill_brewer(type="seq",palette = "YlOrRd")+
    guides(fill=guide_legend(title="Densité (ind/ha)"))+
     geom_sf(data = coastline_loc,
            size = 3, color = "black", fill = "grey")+
    coord_sf()+
    theme_minimal()+
    labs(x = "",
         y = "")+
    theme(plot.title = element_text(hjust = 0.5),
          axis.text = element_text(size = 12),
          legend.text = element_text(size = 12),
          legend.title = element_text(size = 12))
  ggsave(plot = last_plot(), paste0("Distribution of ",species[sp]," juvenils density.png"),
         path = paste0(path_to_figures,"Local scale occurences/Projections"),width = 5.57,height = 5.58, units = "in")
  
  
  ## Compute the frequency of year where the density is superior to the 75th percentile  
  
  # percentile 90 for each layers
  
  value_perc_90 <- quantile(Raster_stack, probs=c(0.90))

  mean(value_perc_90)
  
  ## Give value 1 if its part of the 25% top predictions
  ## Else, give 0
  Raster_stack_freq <- Raster_stack
  
  # values(Raster_stack_freq)[values(Raster_stack_freq) < Q75] <- 0
  # values(Raster_stack_freq)[values(Raster_stack_freq)>= Q75] <- 1
  
   values(Raster_stack_freq)[values(Raster_stack_freq) < value_perc_90[,1]] <- 0
   values(Raster_stack_freq)[values(Raster_stack_freq)>= value_perc_90[,1]] <- 1
  
  ## Compute the frequency of top prediction
  raster_freq <- (sum(Raster_stack_freq)/nlayers(Raster_stack_freq)*100)
  
  
  ggplot()+
    geom_raster(data = as.data.frame(raster_freq,xy=TRUE), mapping = aes(x = x,y = y,fill = layer))+
    scale_fill_viridis_c(na.value = "white",labels=c("0%","25%","50%","75%","100%"))+
    geom_sf(data = coastline_loc,
            size = 3, color = "black", fill = "grey")+
    coord_sf()+
    theme_minimal()+
    labs(x = "",
         y = "",
         fill = "")+
    theme(plot.title = element_text(hjust = 0.5),
          axis.text = element_text(size = 12),
          legend.text = element_text(size = 12),
          legend.title = element_text(size = 12))
    ggsave(plot = last_plot(), paste0("Frequency of ",species[sp]," predictions.png"),
         path = paste0(path_to_figures,"Local scale occurences/Projections"),width = 5.57,height = 5.58, units = "in")
  
  

    ### Plot MESS
    
    mean_MESS <- mean(MESS_stack)
    ggplot()+
      geom_raster(data = as.data.frame(mean_MESS,xy=TRUE), mapping = aes(x = x,y = y,fill = layer))+
      scale_fill_viridis_c(na.value = "white")+
      geom_sf(data = coastline_loc,
              size = 3, color = "black", fill = "grey")+
      coord_sf()+
      theme_minimal()+
      labs(x = "",
           y = "",
           fill = "")+
      theme(plot.title = element_text(hjust = 0.5),
            axis.text = element_text(size = 12))

    
    
    
    
    
    ############################################################################
    ############################################################################
    
    ## Plot one year with highest density and one year with lowest density to see 
    ## if there is a different spatial distribution
    
    # if(species[sp]=="Solea solea"){
    #   # for Solea, 2011 looks like a special year with higher predictions and 2013 
    #   
    #   # plot the 2011 year (select the raster layer with highest max value)
    #   max_year_prediction <- max(Raster_stack)
    #   
    #   #Compute the 90 percent
    #   value_perc_90 <- quantile(max_year_prediction, probs=c(0.9))
    #   
    #   values(max_year_prediction)[values(max_year_prediction) < value_perc_90] <- 0
    #   values(max_year_prediction)[values(max_year_prediction)>= value_perc_90] <- 1
    #   
    #   ggplot()+
    #     geom_raster(data = as.data.frame(max_year_prediction,xy=TRUE), mapping = aes(x = x,y = y,fill = layer))+
    #     scale_fill_viridis_c(na.value = "white")+
    #     geom_sf(data = coastline_loc,
    #             size = 3, color = "black", fill = "grey")+
    #     coord_sf()+
    #     theme_minimal()+
    #     labs(x = "",
    #          y = "",
    #          fill = "")+
    #     theme(plot.title = element_text(hjust = 0.5),
    #           axis.text = element_text(size = 12))
    #   ggsave(plot = last_plot(), paste0("Best year for ",species[sp]," predictions (2011).png"),
    #          path = paste0(path_to_figures,"Local scale occurences/Projections"),width = 5.57,height = 5.58, units = "in")
    #   
    #   # plot the 2013 year (select the raster layer with lowest max value)
    #   min_year_prediction <- min(Raster_stack)
    #   
    #   #Compute the 90 percent
    #   value_perc_90 <- quantile(min_year_prediction, probs=c(0.9))
    #   
    #   values(min_year_prediction)[values(min_year_prediction) < value_perc_90] <- 0
    #   values(min_year_prediction)[values(min_year_prediction)>= value_perc_90] <- 1
    #   
    #   ggplot()+
    #     geom_raster(data = as.data.frame(min_year_prediction,xy=TRUE), mapping = aes(x = x,y = y,fill = layer))+
    #     scale_fill_viridis_c(na.value = "white")+
    #     geom_sf(data = coastline_loc,
    #             size = 3, color = "black", fill = "grey")+
    #     coord_sf()+
    #     theme_minimal()+
    #     labs(x = "",
    #          y = "",
    #          fill = "")+
    #     theme(plot.title = element_text(hjust = 0.5),
    #           axis.text = element_text(size = 12))
    #   ggsave(plot = last_plot(), paste0("Worst year for ",species[sp]," predictions (2013).png"),
    #          path = paste0(path_to_figures,"Local scale occurences/Projections"),width = 5.57,height = 5.58, units = "in")
    #   
    #   
    # }

  
  
  
  
  
  
  