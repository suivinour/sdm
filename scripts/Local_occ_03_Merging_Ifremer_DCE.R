# ---------------------------------------------------------------------------- #
#           SUIVI NOUR    Mise a jour de la distribution des nourricerie                 
# ---------------------------------------------------------------------------- #
# 03. Merge Ifremer and DCE data and clean the data
# ---------------------------------------------------------------------------- #

### Calling all of the common object to my analyses
source("./scripts/boot.R")

########################## Merge data #########################################

for(sp in c(1 : length(species))){
  
  ### Download the cruises data
  
  Cruise_data <- read_delim(paste0(path_to_data_tidy,"Occ/Local/",species[sp],"_Cruise_data.csv"), delim = ",")
  
  ### Download the DCE data
  
  DCE_data <- read_delim(paste0(path_to_data_tidy,"Occ/Local/",species[sp],"_DCE.csv"), delim = ",")
  DCE_data$Origin <- "DCE"
  
  ### Create the commun table
  
  Local_occurences_sp <- Cruise_data %>% 
    rbind(DCE_data) %>% 
    dplyr::select(-1)
  
  
  ############################# Cleaning the points on Land ######################
  ## Some observations can be on land (DCE for exemple), so we need to remove those points
  
  ## Transform coastline into SpatialPolygon format
  coastline_loc_sp <- as(coastline_loc, "Spatial")
  ## Set projection format (WGS84)
  proj4string(coastline_loc_sp) = CRS("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0")
  
  #Identify all the points on land (filter = 1)
  Local_occurences_sp$filter <- pointOnLand(tacsat = Local_occurences_sp, 
                                            lands = coastline_loc_sp, 
                                            proj4string = CRS("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"))
  
  #### Checking the outliers
  # ggplot()+
  #   geom_sf(data = coastline_loc,
  #           size = 3, color = "black", fill = "grey")+
  #   ggtitle(paste0("Spatial distribution of ",species[sp])) +
  #   coord_sf()+
  #   theme_bw()+
  #   geom_point(Local_occurences_sp,
  #              mapping = aes(x = Long, y = Lat, colour = filter),
  #              shape = 19)+
  #   # scale_colour_manual(values=c('#8c510a','#d8b365','#f6e8c3','#5ab4ac','#01665e'))+
  #   labs(x = "Longitude",
  #        y = "Latitude")+
  #   theme(plot.title = element_text(hjust = 0.5))
  
  #Remove those outliers
  Local_occurences_sp <- Local_occurences_sp %>% 
    filter(filter == 0) %>% 
    dplyr::select(-filter)
  
  ########################### Remove presence and absence in the same coord for the same date ########
  
  #### In the next steps, i will have to match these presence/absence data with environmental variables coded with raster resolution
  ### 
  #### The grid resolution is 0.028°* 0.028°
  ###
  #### For the same date (Year and Month) each grid cell can contains multiple observation (presence and absence)
  ###
  #### This could lead to a spatial heterogeneity of the data and need to be take into account
  
  ### First step is to download a raster at the desire resolution
  new_grid <- raster(paste0(path_to_data_tidy,
                                "bathy_regrid.tif"))
  
  
  ### The idea is to have a specific number for each cell to identify duplicates
  vals <- c(1:ncell(new_grid))
  new_grid <- setValues(new_grid, vals) ## It is a grid with number from 1 to XXX for each cell
  
  #plot(new_grid)
  
  # For each set of coordinates, i extract the unique number
  Local_occurences_sp$grid <- extract(new_grid, Local_occurences_sp[,c("Long","Lat")])
  
  ### Merge data too close
  Local_occurences_sp <- Local_occurences_sp %>% 
    group_by(Year,Month,grid) %>% 
    # Select in each group, lines with the highest Density values (1 if there is 0 and 1 in the same group)
    slice_max(Density, n=1) %>% # the mean is also possible
    # Select the first occurence in each group
    slice_head(n=1) %>% 
    ungroup() %>% 
    # Remove the grid identifier
    dplyr::select(-grid)
  
  
  ### Save dataframe
  
  write.csv(Local_occurences_sp, file = paste0(path_to_data_tidy,"Occ/Local/",species[sp],"_Local_occurences.csv"))
}
