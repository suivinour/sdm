Organisation du disque SuiviNour

02/02/2023 : creation GitLab

- SDM_Nourricerie

- R : L'ensemble des scripts et analyses réalisées dans le cadre du stage/CDD du projet SuiviNour
	SuiviNour : Projet R 
	
	
		data
			raw
				coastline_outline
					
				Env_data
					Copernicus
					SHOM
					gebco
				Occ
					Regional
					Local
			
			tidy
			
				Env_data
					Copernicus
				Occ
					Regional
					Local
					
				coastline_outline	
			
		docs
		figures
		scripts
		
		Notation type : Echelle_typeanalyse_Numeroscript_fonction.R
			
			boot.R : All the common variables between each scripts 
			function : Call all the common functions between each script
		
	## Species distribution in North Atlantic	
		## Download environmental data (Required)
			- Regional_env_01_Copernicus_Download_data_GLOBAL_ANALYSIS_FORECAST_BIO_001_028.R  :  This script extract from Copernicus the Chla, O2 and NPP for the 2020 - 2022 period in the North Atlantic with a monthly resolution
			
			- Regional_env_02_Copernicus_Download_data_GLOBAL_ANALYSISFORECAST_PHY_001_024.R  :  This script extract from Copernicus the Salinity and Temperature for the 2020 - 2022 period in the North Atlantic with a monthly resolution

			- Regional_env_03_Copernicus_Download_data_GLOBAL_MULTIYEAR_BGC_001_029-TDS.R  :  This script extract from Copernicus the Chla, O2 and NPP for the 1993 - 2020 period in the North Atlantic with a monthly resolution

			- Regional_env_04_Copernicus_Download_data_GLOBAL_MULTIYEAR_PHY_001_030.R  :  This script extract from Copernicus the Salinity and Temperature for the 1993 - 2020 period in the North Atlantic with a monthly resolution

		## Process environmental data (Required)		
			- Regional_env_05_Copernicus_prepare_grid_file_for_interpolation.R  :  Creation of a grid with the target resolution for future interpolation

			- Regional_env_06_Copernicus_extract_bottom_and_interpolate.R  :  Extraction of bottom layer for Salinity and Oxygen and linear interpolation of missing points

			- Regional_env_07_Copernicus_interpolate_surface_layer.R  :  Extraction of surface layer for Chlorophylle,  NPP, Salinity & Oxygen and linear interpolation of missing points

			- Regional_env_07bis_Copernicus_interpolate_plot_all_layer.R  : ADDITIONAL SCRIPT // Plot the previous interpolated layers  

			- Regional_env_08_Copernicus_interpolate_bottom_layer.R  : Interpolation of temperature and salinity product with already bottom layers 

			- Regional_env_09_Compute_Chl_nppv_seasonal_mean.R  : This script create an annual seasonal layer of chlorophylle and NPP for the 1993 - 2022 by taking the mean of April - Septembre

			- Regional_env_10_Stack_environment_for_projection.R  : This script create a stack of environmental layers for each month. These stack will be used for projection

		## Complementary analyses (Optional)
			- Regional_env_11_Analysis_of_temperature_evolution.R : This script test the evolution of bottom temperature in the 0-50m of the Bay of Biscay. 
										We look at the evolution of annual temperature and also at the seasonal decomposition of this variable

			- Regional_env_12_Projection_of_temp_and_oxygen_evolution  : This script plot the temperture and oxygen layer in the Bay of Biscay for different season

		## Process biological data (Required)
			- Regional_occ_01_OBIS_GBIF_extract.R  : This script extract the occurences of a list of species from OBIS and GBIF and delete the duplicated values

			- Regional_occ_02_Ifremer_Cruse_extract.R  : This script extract the occurences of a list of species from different scientific cruises and reshape the data

			- Regional_occ_03_Merging_Ifremer_OBIS__GBIF.R  : The data from OBIS, GBIF and scientific cruises are merged with a regard on duplicated occurences

			- Regional_occ_04_Mapping_occ.R  : ADDITIONAL SCRIPT // Plots to represent the spatial and temporal distribution of the data 

			- Regional_occ_05_Gridding occurences.R  : ADDITIONAL SCRIPT // Plots to represent the recurrency of presence and absence in a grid  

		## Combine Biological and Environmental data (Required)			
			Regional_occ_06_Combine_occurences_with_environment.R  : This script retrieve the environmental value for each observation. Nearest neighbor methods are realized for points
										without environmental value.

			Regional_occ_07_Preparation_of_data_before_modelisation.R  : Resampling data // Compute Oxygen saturation // Test the multicolinearity between environmental variables 

			Regional_occ_08_Plot_data_before_modelisation.R  : ADDITIONAL SCRIPT // Plots to represent the spatial and temporal distribution of the final data 

		## Modelisation (Required)	
			Regional_mod_01_Ensemble_model_construction.R  :  This script compute and evaluate 5 individual models with 10 repetition. An ensemble model is then perform on these 50 models. 
									This can take a long time to run if you choose GAM model(~4 hours)

			Regional_proj_01_Projection_of_ensemble_model.R : Project each individual model for each month of each year (~20000 projection). An ensemble projection of the ensemble model is then done for each month (~380 projection)
									This code is really long to run (~5 days). Highly recommented to be run on Datarmor and to think the modelisation objectives thanks to this computation time

			Regional_proj_02_Compute_statistics.R  : This script transform the mean, the standard deviation and the MESS of each season (Winter / Summer)

			Regional_proj_03_Plot_species_distribution.R  : ADDITIONAL SCRIPT // Plots to projections

	## Nursery habitat modelisation in the Bay of Biscay

		## Download environmental data (Required)

			Local_env_01_Download_Copernicus_data_ IBI_MULTIYEAR_PHY_005_002.R  : Extract from Copernicus the Temperature and Salinity for the 1993 - 2021 period in the Bay of Biscay with a daily resolution

			Local_env_02_Download_Copernicus_data_IBI_MULTIYEAR_BGC_005_003.R : Extract from Copernicus the Chla, O2 and NPP for the 1993 - 2021 period in the Bay of Biscay with a daily resolution

			Local_env_03_Download_Copernicus_data_IBI_MULTIYEAR_WAV_005_006.R  : Extract from Copernicus the Wave height for the 1993 - 2021 period in the Bay of Biscay with a daily resolution

		## Process environmental data (Required)

			Local_env_04_Copernicus_get_bottom_layer.R  : Get bottom layer for salinity and oxygen

			Local_env_05a_Copernicus_provide_metrics_month_bottom_oxygen_product.R  : This script computes oxygen saturation and transform the variable from daily scale to May - August mean

			Local_env_05b_Copernicus_provide_metrics_month_bottom_salinity_product.R  : This script computes mean, variation coefficient, upper percentile of salinity and days below 10 psu between May - August

			Local_env_05c_Copernicus_provide_metrics_month_bottom_temperature_product.R  : This script computes mean, variation coefficient, upper percentile of bottom temperature between May - August

			Local_env_05d_Copernicus_provide_metrics_month_chl_nppv_product.R  : This script computes the mean of Chl and NPP for the April to Septembre period

			Local_env_05e_Copernicus_provide_metrics_month_product.R

			Local_env_06_Transform_wave_height_product.R  : This script computes the mean of wave mean for the May - August period

			Local_env_07_Copernicus_prepare_grid_file_for_interpolation.R  : Creation of a grid with the target resolution for future interpolation

			Local_env_08_Copernicus_extract_bottom_and_interpolate.R  : 

			Local_env_09_Copernicus_interpolate_surface_layer.R  : This script interpolate each variables

			Local_env_10_Copernicus_interpolate_plot_all_layer.R  : ADDITIONAL SCRIPT // Plot the previous interpolated layers  

		## Process biological data (Required)
			Local_occ_01_Ifremer_Cruise_extract.R : Extraction and filtering of scientific cruises occurences of juvenils in the Bay of Biscay for the species of interest

			Local_occ_02_DCE_extract.R : Extraction and filtering of DCE (French programm of river and estuary observation) occurences of juvenils in the Bay of Biscay for the species of interest

			Local_occ_03_Merging_Ifremer_DCE.R : Combine the DCE and the Ifremer cruises in the Bay of Biscay and removing the points on land

			Local_occ_04_Mapping_YoY : ADDITIONAL SCRIPT // Plots to represent the spatial and temporal distribution of the data 

		## Combine Biological and Environmental data (Required)	
			Local_occ_05_Combine_occurences_with_environment.R  : This script combine each observation with the different variables (continuous) // We also create a stack of environment for each year in order to do the projection in the futur
										IMPORTANT : Use this script if you want to use continuous variables for the modelisation (for BRT model per example)

			Local_occ_05bis_Combine_occurences_with_categorical_environment.R  : This script combine each observation with the different variables (categorical) // We also create a stack of environment for each year in order to do the projection in the futur
										IMPORTANT : Use this script if you want to use categorical variables for the modelisation (for Delta model per example)

			Local_occ_06_Preparation_of_data_before_modelisation.R  : This script test the collinearity and the VIF between each variables 

			Local_occ_06bis_Represent_data_in_categories.R  : ADDITIONAL SCRIPT // The script plot the density of juvenils by categories of variables 

		## Modelisation (Required)
			Local_occ_07_Categorical_Delta_model.R  : This script computes the delta model (binomial + gaussian GLM) for the dataset with categories. No separation is made between calibration and validation dataset. If you want to do so, look at WorkInProgess/Local_occ_07_Delta_model
			
			Local_occ_08_Projection_delta_model.R  : This script project the selected delta model for each year during the 1993 - 2021 period. 
								I compute the mean density over the period, the standard deviation , the MESS (extrapolation) and the frequency of top density



- Rapports
- Biblio
- Administrateif