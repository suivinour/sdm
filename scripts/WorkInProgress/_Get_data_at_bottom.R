# ---------------------------------------------------------------------------- #
#           SUIVI NOUR    Mise a jour de la distribution des nourricerie                 
# ---------------------------------------------------------------------------- #
# This script is to obtain bottom variables for product : Copernicus_GLOBAL_MULTIYEAR_PHY_001_030
# ---------------------------------------------------------------------------- #

source(paste0("./scripts/boot.R"))

# load bathymetry mask downloaded from copernicus statistic

mask_copernicus<-brick(paste0(path_to_data_raw, 'GLO-MFC_001_030_mask_bathy.nc'))

mask_copernicus<-crop(mask_copernicus, 
                      c(min_x_reg,
                        max_x_reg,
                        min_y_reg,
                        max_y_reg))



# Get round bathymetry cathegories
depth_cath<-round(getZ(mask_copernicus),
                  digits = 0)

mask_layer<-mask_copernicus
values(mask_layer)<-NA # put NA in each layer to be sure previous values remained

##################Assign at each level the values of the depth

#we start from the deeper layer to use it as a mask in de the upper layer

depth_merge_list<-lapply(length(names(mask_copernicus)):1, function(depth_cath_i){
  
  if(depth_cath_i==length(names(mask_copernicus))){ 
  
  print(paste0('Depth---- ', getZ(mask_copernicus[[depth_cath_i]]), ' meters'))
    
  mask_layer_i<-mask_copernicus[[depth_cath_i]]
  mask_layer_i[mask_layer_i == 1] <- getZ(mask_copernicus[[depth_cath_i]])
  mask_layer_i[mask_layer_i == 0] <- NA

  
  }else{
  
    print(paste0('Depth---- ', getZ(mask_copernicus[[depth_cath_i]]), ' meters'))
    
    mask_layer_i<-mask_copernicus[[depth_cath_i]]
    # Assign depth values to the layer
    mask_layer_i[mask_layer_i ==1] <- getZ(mask_copernicus[[depth_cath_i]]) 
    mask_layer_i[mask_layer_i ==0] <- NA
    # Use the deeper layer as a mask
    layer_n_sup<-mask_copernicus[[depth_cath_i+1]]
    layer_n_sup[layer_n_sup==0]<-(-999)
    layer_n_sup[layer_n_sup==1] <- NA
    if (length(unique(values(layer_n_sup-mask_layer_i)))<2){ #If the layer has only NA -> no bottom are close to the coast
    values(mask_layer_i)<-0
    }
    #plot( mask_copernicus[[depth_cath_i]], 
    #     col='red',
    #     main=getZ(mask_copernicus[[depth_cath_i]]))  # check 
}
  return( mask_layer_i )
  })

# stack the obtained list
depth_merge_stack<-stack(depth_merge_list)

# Combine all layers
depth_merge_rast <- merge(depth_merge_stack)
plot(depth_merge_rast)# check

# Remove continent
depth_merge_rast<-mask(depth_merge_rast, 
                       mask_copernicus[[1]])

depth_merge_rast[depth_merge_rast==0]<-NA


################" Keep only netcdf at bottom (exclusion of 0, 2 , 3 meters here)

# Extract bathymetry values from the depth_merge_rast and round
depth_keep_round<-na.omit(round(unique(values(depth_merge_rast)), digits = 0))
# Put in order 
depth_keep_round<-depth_keep_round[order(depth_keep_round)]
# Same nut with extact values
depth_exact<-sort(as.numeric(na.omit(unique(values(depth_merge_rast)))))

# Give the product you want
prod_i<-"Copernicus_GLOBAL_MULTIYEAR_PHY_001_030_so"

# Path to the product
path_to_data_raw_copernicus<-paste0(path_to_data_raw,
                                "Env_data/Copernicus/", prod_i, "/")

path_to_data_tidy_copernicus<-paste0(path_to_data_raw,
                                    "Env_data/Copernicus/", prod_i, "/")
dir.create(paste0(path_to_data_tidy_copernicus) , showWarnings = F)

# Give netcdf list in the folder
ncdf.list<-list.files(path_to_data_raw_copernicus)

# Extract the depth
split.name<-str_split(ncdf.list, "_") 
split.name<-do.call(rbind, split.name)
# The depth is at the 8th position, I don't know how to automatize
depth_ncdf<-as.numeric(split.name[,8][order(as.numeric(split.name[,8]))])

# Extract files names used for bottom (exclusion of 0, 2 and 3 meters here)
ncdf.list.order<-ncdf.list[order(as.numeric(split.name[,8]))]
ncdf.list.order<-ncdf.list.order[depth_ncdf %in% depth_keep_round]


#################### Create a file with only bottom values in one single list

list.stack<-lapply(1:length(ncdf.list.order),function(ncdf_i) {

  ncdf_name<-ncdf.list.order[ncdf_i]
  bottom_var<-depth_merge_rast #We use it as a mask and put NA for all values are not the depth target
 
  print(paste0( ncdf_name, " --- initiated-------","Depth exact is : ", depth_exact[ncdf_i])) # check if the depth is in the same order or all downloaded ( Need to add a check for this)

  bottom_var[bottom_var==depth_exact[ncdf_i]]<-(-999)
  bottom_var[bottom_var!=(-999)]<-NA
  

  env_rast_init<-brick(paste0(path_to_data_raw_copernicus,"/",  ncdf_name))
  time.list<-getZ( env_rast_init)
  env_rast_init<-terra::crop(env_rast_init, 
                     extent(bottom_var))
  
  env_rast_init<-setZ(env_rast_init, time.list)

  env_rast_at_bottom<-raster::mask(env_rast_init,
                                   bottom_var)
  
  
  
    return(env_rast_at_bottom)

 
  
  
rm(bottom_var)
})

#Extract the dates of each files (normally identical)
list.date<-getZ(list.stack[[1]])

################# Extract bottom at each level of bathy and merge to have one raster 
# for on date for bottom
bottom_raster_list<-lapply(1:length(list.stack), function(date_i){

 bottom_at_date<- merge(stack(lapply(list.stack ,"[[", date_i)))
 names(bottom_at_date)<-list.date[date_i]
 return(bottom_at_date) 

})  

#Stack the raster list
bottom_stack_final<-stack(bottom_raster_list)


writeRaster(bottom_stack_final, paste0(path_to_data_tidy))
