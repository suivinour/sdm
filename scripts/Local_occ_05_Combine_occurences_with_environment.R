# ---------------------------------------------------------------------------- #
#           SUIVI NOUR    Mise a jour de la distribution des nourricerie                 
# ---------------------------------------------------------------------------- #
# 05. Combine density with environmental values
# ---------------------------------------------------------------------------- #

### The goal of this script is to match the occurences with all of the environmental 
## layers (Oxygen, Salinity,  Temperature, Substrate, Wave, Chl and NPP) for the local scale

### For the density of juvenils, i decide to focus on the first 50m as no G0
### are expected deeper

### Calling all of the common object to my analyses
source("./scripts/boot.R")

##----------------------- Import environmental layers --------------------------

# Import bathymetry
bathy_regrid <- raster(paste0(path_to_data_tidy,
                              "bathy_regrid.tif"))

# Import bathymetry below 50
bathy_regrid_50 <- raster(paste0(path_to_data_tidy,
                                 "bathy_regrid_50.tif"))


################ Download substrate and add correction #########################
Substrate <- st_read(paste0(path_to_data_raw,
                            "Env_data/SHOM/SEDIM_Mondiale/SHAPE/CarteSedimMonde_2021.shp")) %>% 
  dplyr::select(-Numero)

## The type if substrate is not encoded in a clear name format 
(Type <- unique(Substrate$TYPE_VALEU))

## Import the correction dataset created by myself
Correction <- read.csv(paste0(path_to_data_raw,
                              "Env_data/SHOM/SEDIM_Mondiale/Substrat_type_correction.csv"), sep = ";")

Substrate_modified <- left_join(Substrate, Correction) %>% 
  dplyr::select(-TYPE_VALEU) 

Substrate_modified$Substrat <- as.factor(Substrate_modified$Substrat)

##Convert substrat into a raster
Substrate_rast <- fasterize(Substrate_modified,bathy_regrid_50, field = "Substrat")

## Use the bathymetry to mask the substrate
Substrate_rast <- mask(Substrate_rast,bathy_regrid_50)


############## Calling the path for each Copernicus product ####################

chl_mean <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_chl_mean_May_August/",
                   list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_chl_mean_May_August/"),
                              pattern = ".grd"))

nppv_mean <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_nppv_mean_May_August/",
                    list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_nppv_mean_May_August/"),
                               pattern = ".grd"))

o2_mean <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_o2_bottom_mean_May_August/",
                  list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_o2_bottom_mean_May_August/"),
                             pattern = ".grd"))


o2_cv <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_o2_bottom_cv_May_August/",
                list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_o2_bottom_cv_May_August/"),
                           pattern = ".grd"))

o2_perc <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_o2_bottom_perc_25_May_August/",
                  list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_BGC_005_003-TDS_o2_bottom_perc_25_May_August/"),
                             pattern = ".grd"))

temp_mean <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_bottomT_mean_May_August/",
                    list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_bottomT_mean_May_August/"),
                               pattern = ".grd"))

temp_cv <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_bottomT_cv_May_August/",
                  list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_bottomT_cv_May_August/"),
                             pattern = ".grd"))

temp_perc_90 <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_bottomT_perc_90_May_August/",
                       list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_bottomT_perc_90_May_August/"),
                                  pattern = ".grd"))

sali_mean <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_so_bottom_mean_May_August/",
                    list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_so_bottom_mean_May_August/"),
                               pattern = ".grd"))

sali_cv <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_so_bottom_cv_May_August/",
                  list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_so_bottom_cv_May_August/"),
                             pattern = ".grd"))

sali_tresh_10 <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_so_bottom_tresh_10_May_August/",
                        list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_so_bottom_tresh_10_May_August/"),
                                   pattern = ".grd"))

sali_perc_90 <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_so_bottom_perc_90_May_August/",
                       list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_PHY_005_002-TDS_so_bottom_perc_90_May_August/"),
                                  pattern = ".grd"))

wave_mean <- paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_WAV_005_006-TDS_VHM0_mean_May_August/",
                    list.files(paste0(path_to_data_tidy,"Env_data/Copernicus_interpolated/Copernicus_IBI_MULTIYEAR_WAV_005_006-TDS_VHM0_mean_May_August/"),
                               pattern = ".grd"))


###------------------- Merging data and Environment ----------------------------
#-------------------------------------------------------------------------------


## Loop for each species

for (sp in (1:length(species))) {
 
  ### Import Occurences
  
  Occurences <- read_delim(paste0(path_to_data_tidy,"Occ/Local/", species[sp],"_Local_occurences.csv"),delim = ",", col_select = c(2:11))
  
  
  ######################## Bathymetry ##########################################
  ###
  # Extract the bathymetry for each point. We will model the distribution for data 
  # that are below 50 meters depth
  
  # First, give bathymetry to each point
  Occurences$bathymetry <- raster::extract(bathy_regrid, Occurences[,c("Long","Lat")])
  
  # For the points without values, we find the closest point with values
  Occurences_NA <- Occurences %>% filter(is.na(bathymetry))
  
  # To perform the closest neighbor value extraction, the bathymetry raster needs to be as a data frame
  # As the missing points are close to shore, we take bathy_regrid_50 to reduce computation
  Raster_xy <- as.data.frame(bathy_regrid_50, xy=TRUE, na.rm=TRUE)
  
  # Compute the Euclidean distance between each NA points with k neigbours
  closest <- nn2(data=Raster_xy[,1:2], query = Occurences_NA[,c("Long","Lat")], k=1)
  
  # Transform the data frame in order to bind with other values
  Occurences_NA <- cbind(Raster_xy[closest$nn.idx,],Occurences_NA)
  Occurences_NA$bathymetry <- Occurences_NA[,3]
  Occurences_NA <- Occurences_NA %>% dplyr::select(-c(1:3))
  
  # Bind the occurences
  Occurences <- Occurences %>% 
    rbind(Occurences_NA) %>% 
    filter(is.na(bathymetry)== FALSE)
  
  ### Select the occurences below 50 m 
  Occurences <- Occurences %>% filter(bathymetry >= -50)
  
  ######################## Substrat ##########################################
  ##
  
  # Extract the substrat for each point
  Occurences$substrat <- raster::extract(Substrate_rast, Occurences[,c("Long","Lat")])
  
  # For the points without values, we find the closest point with values
  Occurences_NA <- Occurences %>% filter(is.na(substrat))
  
  # To perform the closest neighbor value extraction, the bathymetry raster needs to be as a data frame
  Raster_xy <- as.data.frame(Substrate_rast, xy=TRUE, na.rm=TRUE)
  
  # Compute the Euclidean distance between each NA points with k neigbours
  closest <- nn2(data=Raster_xy[,1:2], query = Occurences_NA[,c("Long","Lat")], k=1)
  
  # Transform the data frame in order to bind with other values
  Occurences_NA <- cbind(Raster_xy[closest$nn.idx,],Occurences_NA)
  Occurences_NA$substrat <- Occurences_NA[,3]
  Occurences_NA <- Occurences_NA %>% dplyr::select(-c(1:3))
  
  # Bind the occurences
  Occurences <- Occurences %>% 
    rbind(Occurences_NA) %>% 
    filter(is.na(substrat)== FALSE)
  
  ## Set Substrat as factor
  Occurences$substrat <- as.factor(Occurences$substrat)
  
  ## Set Substrat map as factor
  values(Substrate_rast) <- factor(values(Substrate_rast))
  
  
  ########################### Copernicus Variables #############################
  ##############################################################################
  
  ## As we decide to download each variable for the May - August period of each year, we need
  ## to loop the attribution of each variable by year
  
  ## In the same time, we prepare the raster stack of current environment for the projection
  
  ## Initialization
  year <- c(1993 : 2021)
  Final_occurence <- c()
  
  for(year_i in (1 : length(year))){
    
    ## Select the occurences for this date
    Occurence_date <- Occurences %>% 
      filter(Year == year[year_i]) 
    
    
      ### Stack each environmental variables
      Current_env <- stack(chl_mean[year_i],
                           nppv_mean[year_i],
                           o2_mean[year_i],
                           o2_cv[year_i],
                           o2_perc[year_i],
                           temp_mean[year_i],
                           temp_cv[year_i],
                           temp_perc_90[year_i],
                           sali_mean[year_i],
                           sali_cv[year_i],
                           sali_tresh_10[year_i],
                           sali_perc_90[year_i],
                           wave_mean[year_i]
      )
      # Rename raster layers
      names(Current_env) <- c("chl_mean","nppv_mean","o2_mean","o2_cv","o2_perc_25","temp_mean",
                              "temp_cv","temp_perc_90","sali_mean","sali_cv","sali_tresh_10",
                              "sali_perc_90","wave_mean")
      
      ## I figure out during the process that wave height as been considered as 
      ## a numeric but it should be a factor (1 = less exposed, 3 = very exposed)
      values(Current_env[["wave_mean"]]) <- round(values(Current_env[["wave_mean"]]), digits = 0)
      values(Current_env[["wave_mean"]])[values(Current_env[["wave_mean"]])==0] <- 1
      values(Current_env[["wave_mean"]]) <- factor(values(Current_env[["wave_mean"]]))
      
      
      if(nrow(Occurence_date)>=1){
      Values <- raster::extract(Current_env, Occurence_date[,c("Long","Lat")])
      
      # bind the extracted values with the occurence dataframe
      Occurence_date <- cbind(Occurence_date,Values)
      
      # For the points without values, we find the closest point with values
      Occurence_NA <- as.data.frame(Occurence_date) %>% filter(is.na(chl_mean))
      
      # To perform the closest neighbor value extraction, the bathymetry raster needs to be as a data frame
      Raster_xy <- as.data.frame(Current_env, xy=TRUE, na.rm=TRUE)
      
      # Compute the Euclidean distance between each NA points with k neigbours
      closest <- nn2(data=Raster_xy[,1:2], query = Occurence_NA[,c("Long","Lat")], k=1)
      
      # Transform the data frame in order to bind with other values
      Occurence_NA <- cbind(Raster_xy[closest$nn.idx,],Occurence_NA)
      
      # Replace the closest value in the right position
      Occurence_NA[,c(28:39)] <- Occurence_NA[,c(3:14)]
      
      Occurence_NA <- Occurence_NA %>% dplyr::select(-c(1:15))
      
      # Bind the occurences
      Occurence_date <- Occurence_date %>% 
        rbind(Occurence_NA) %>% 
        filter(is.na(chl_mean)== FALSE)
      
      #### This operation needs to be repeted to wave_mean as some values are still missing
      if(sum(is.na(Occurence_date$wave_mean))>0){
      Occurence_NA <- as.data.frame(Occurence_date) %>% filter(is.na(wave_mean))
      
      Raster_xy <- as.data.frame(Current_env[["wave_mean"]], xy=TRUE, na.rm=TRUE)
      
      closest <- nn2(data=Raster_xy[,1:2], query = Occurence_NA[,c("Long","Lat")], k=1)
      
      Occurence_NA <- cbind(Raster_xy[closest$nn.idx,],Occurence_NA)
      
      Occurence_NA[,c("wave_mean")] <- Occurence_NA[,c("wave_mean_VALUE")]
      
      Occurence_NA <- Occurence_NA %>% dplyr::select(-c("x","y","wave_mean_VALUE"))
      
      Occurence_date <- Occurence_date %>% 
        rbind(Occurence_NA) %>% 
        filter(is.na(wave_mean)== FALSE)
      }
      
      # Set wave_mean as factor
      Occurence_date$wave_mean <- factor(Occurence_date$wave_mean)
      
      
      Final_occurence <- rbind(Final_occurence,Occurence_date)
      
      }
      ### Add the bathymetry and the substrat map to the Current_env stack and save them in a 
      ### new output,
      
      Current_env <- stack(Current_env, bathy_regrid_50,Substrate_rast)
      
      names(Current_env) <- c("chl_mean","nppv_mean","o2_mean","o2_cv","o2_perc_25","temp_mean",
                              "temp_cv","temp_perc_90","sali_mean","sali_cv","sali_tresh_10",
                              "sali_perc_90","wave_mean","bathymetry","substrat")
      
      # Create folder
      folder_output <- paste0(path_to_data_tidy,
                              "Env_data/Local_Projection_Environment")
      
      dir.create(folder_output,
                 recursive = T,
                 showWarnings = F)
      
      # Save raster
      writeRaster(Current_env, paste0(folder_output,"/Environment_",year[year_i],".grd"),
                  overwrite = TRUE)
    
  } 
  
  #Save occurence data
  
  write.csv(Final_occurence, file = paste0(path_to_data_tidy,"Occ/Local/",species[sp],"_Local_scale_occurences_env.csv"))
}
