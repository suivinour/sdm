## This script use the function OccGbifObis to search all the occurences of a species list in OBIS and GBIF
## to create data table of occurences without duplicates

source("./scripts/boot.R")

###################Create a WKT polygon of the region###################################

Atlantic = wkt("POLYGON ((-6 36, 9 52, 31 56, 31 72, -36 72, -36 36, -6 36))")

################################# Search all the occurence in OBIS/GBIF #########################

Occ <- OccGbifObis(specieslist = species, selObis = T,selGbif = T,SEL.MAP = F,geometry = Atlantic)

# Save data set

for(sp in c( 1 : length(species))){
  
  table <- as.data.frame(Occ[[species[sp]]]["TOTALformat"])
  
  colnames(table) <- c("Long","Lat","Country", "InstitutionCode","Year","Month","DatasetName","CollectionCode")
  
  table$Origin <- "OBIS_GBIF"
  
  write.csv(table, paste0(path_to_data_raw,"Occ/Regional/",species[sp],"_OBIS_GBIF_raw.csv"))
}

#### Check the proportion of data that are out of the North East Atlantic

## Total_Occ <- OccGbifObis(specieslist = species, selObis = T,selGbif = T,SEL.MAP = F,geometry = NULL)

## For S.solea, there is 81 188 occurences in the global dataset and 78 727 in our North East Atlantic restriction (~97%)
## For M.merlangus, there is 687 467 occurences in the global dataset and 687 107 in our North East Atlantic restriction (~99%)

# We do not miss any important information in these datasets




