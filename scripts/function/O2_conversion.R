## Conversion of mg/l o2 in %saturation
# te = temperature in degree
# ox = oxygen in mg/L
# sa = salinity

conv_mg_sat = function(te,ox,sa) {
  
  pp = ox/1.429
  oo =  (100*pp)/(0.0223916*(exp(-135.90205+
                               ((1.575701*10^5)/(te+273.15))-
                               ((6.642308*10^7)/(te+273.15)^2)+
                               ((1.2438*10^10)/(te+273.15)^3)-
                               ((8.621949*10^11)/(te+273.15)^4)-
                               sa*(0.017674-
                                     (10.754/(te+273.15))+
                                     (2140.7/(te+273.15)^2)))))
  return(oo)
  
}
